'use strict';
var exports = module.exports = {};

var pdf = require('html-pdf');
var pug = require('pug');
var async = require('async');
var dateFormat = require('dateformat');
var moment = require('moment');

//Compile ticket template
var compiledTicket = pug.compileFile(__dirname + "/../views/ticket.pug");
var attachments = [];
var evnt;

exports.getPdfs = function(order, event, callback) {
    evnt = event;
    //Clear the attachments array before proceeding
    attachments = [];
    async.each(order.tickets, getPdf, function(err) {
        if (err) { return callback(err); }

        callback(null, attachments);
    });
};

function getPdf(ticket, cb) {
    //FORMAT THAT BLOODY DATE
    //var s = moment("17/11/2017 9:00 PM", "DD-MM-YYYY h:mm A");
    var dt = evnt.startDate.Date + " " + evnt.startDate.Time;
    var s = moment(dt, "DD-MM-YYYY h:mm A");
    var eventDate = dateFormat(s, "dddd, mmmm dS, yyyy");
    var time = dateFormat(s, "shortTime");

    var ticketHtml = compiledTicket({
        ticketID: ticket.ticketID,
        ticketType: ticket.ticketType,
        price: ticket.price,
        evntName: evnt.eventName,
        location: evnt.eventLocation,
        date: eventDate,
        time: time
    });
    const port = process.env.PORT;
    pdf.create(ticketHtml, {
        "format": "Legal",
        "base": 'http://localhost:' + port,
        "orientation": "landscape"
    }).toBuffer(function(err, pdf) {
        if (err) { return cb(err); }
        var b = new Buffer(pdf);
        var attachment = {
            filename: ticket.ticketID + '.pdf',
            content: b
        };
        attachments.push(attachment);
        return cb(null);
    });
}