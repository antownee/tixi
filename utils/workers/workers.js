'use strict';

const mailer = require("../../utils/mailer");
const pdfAttachment = require("../../utils/ticket");
const async = require('async');
const pug = require('pug');
const logger = require('../logger');
const kue = require('kue');
const jobs = kue.createQueue(),
    Job = kue.Job;


//Compile email template
var compiledEmail = pug.compileFile(__dirname + "/../../views/email3.pug");

var exports = module.exports = {};


exports.create = function(jobTitle, jobData) {
    var job = jobs.create(jobTitle, jobData);

    job.on("failed", () => {
        console.log("Job failed.");
    }).on("progress", (progress) => {
        console.log(`Processing job - ${progress}`);
    });

    job.save();
};

jobs.process("emails", 3, (job, done) => {
    console.log(`job: Processing ${job.data.title}. JOB ID: ${job.id}.`);
    
    //Do the work here
    sendEmail(job.data.order, job.data.event, done);
});

jobs.on("job complete", (id) => {
    Job.get(id, (err, job) => {
        if (err) { return; }

        job.remove((err1) => {
            if (err1) { return; }
            console.log(`job: Completed ${job.id}`);
        });

    });
});


function sendEmail(ordr, event, done) {
    async.waterfall([
        function(callback) {
            pdfAttachment.getPdfs(ordr, event, callback);
        },
        function(attachmnts, callback) {
            if (!ordr.orderComplete) {
                logger.transactionLogger.info(`Unable to send email because order ${ordr.orderID} is incomlete.`);
                console.log("Incomplete order");
                return new Error(`Unable to send email because order ${ordr.orderID} is incomplete`);
            }

            logger.transactionLogger.info("PDF attachments ready.");

            var mailOptions = {
                from: '"Tixi" <hello@sendy.tixi.co.ke>',
                to: ordr.email,
                subject: event.eventName + ' tickets!',
                html: compiledEmail({ fullName: ordr.name.first, festivalName: event.eventName, tickets: ordr.tickets }),
                attachments: attachmnts
            };
            logger.transactionLogger.info("Preparing email options");
            mailer.sendMail(mailOptions, 'TICKET EMAIL', callback);
        }
    ], function(err) {
        if (err) {
            done(err);
        }
        done();
    });
}

function sendText(){
    //Notify user to check their email for the received tickets
}