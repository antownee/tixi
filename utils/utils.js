'use strict';

var exports = module.exports = {};

var dateFormat = require('dateformat');
var moment = require('moment');

exports.getDate = function(date, time) {
    var dt = `${date} ${time}`;
    var s = moment(dt, "DD-MM-YYYY h:mm A");
    var eventDate = dateFormat(s, "dddd, mmmm dS, yyyy");
    return eventDate;
};

exports.getShortTime = function(date, time) {
    var dt = `${date} ${time}`;
    var s = moment(dt, "DD-MM-YYYY h:mm A");
    var tahm = dateFormat(s, "shortTime");
    return tahm;
};

exports.getShortDate = function(date) {
    var dt = `${date}`;
    var s = moment(dt, "DD-MM-YYYY h:mm A");
    var eventDate = dateFormat(s, "dddd, mmmm dS, yyyy");
    return eventDate;
};