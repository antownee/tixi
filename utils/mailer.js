'use strict';
var exports = module.exports = {};

const nodemailer = require('nodemailer')
const config = require("../config/config")
const logger = require('../utils/logger')
const moment = require('moment');
const mg = require('nodemailer-mailgun-transport');

//Setting up node mailer transport
var auth = {
    auth: {
        api_key: config.mailgun.api_key,
        domain: config.mailgun.domain
    }
}

var transporter = nodemailer.createTransport(mg(auth));

exports.sendMail = function(mailOptions, action, callback) {
    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            logger.transactionLogger.info("SENT: NO; EMAIL: " + mailOptions.to + " ACTION: " + action);
            logger.unsentEmailLogger.info("UNSENT EMAIL TO : " + mailOptions.to);
            return callback(err);
        }

        //LOG THIS TO THE EMAILS SENT LOG and update db with email sent
        logger.transactionLogger.info("EMAIL SENT: " + mailOptions.to + " ACTION: " + action + " INFO: " + info.id);
        logger.sentEmailLogger.info("EMAIL SENT TO : " + mailOptions.to);
        return callback(null);
    });
};