'use strict';

var exports = module.exports = {};

const config = require("../config/config")
const util = require('util');
const lipisha = require("lipisha");
const logger = require('../utils/logger');


//Utility function
function Padder(len, pad) {
    if (len === undefined) {
        len = 1;
    } else if (pad === undefined) {
        pad = '0';
    }

    var pads = '';
    while (pads.length < len) {
        pads += pad;
    }

    this.pad = function(what) {
        var s = what.toString();
        return pads.substring(0, pads.length - s.length) + s;
    };
}
var pd = new Padder(6);

//Lipisha Setup
var lipishaClient = new lipisha.Lipisha(config.lipisha.sandboxKey, config.lipisha.sandboxSignature, 'test');

exports.authoriseLipisha = function(ccdetails) {
    var cno = ccdetails.cno;
    var cvc = ccdetails.cvc;
    var ex = ccdetails.ex;
    var amnt = ccdetails.amnt;
    var ph = ccdetails.phone;
    var fullname = ccdetails.fullname;
    var accNo = config.lipisha.accountNumber;

    //Transform them tingz.
    cno = cno.replace(/\s/g, '');
    ex = ex.replace(/[^0-9\.]+/g, "");
    var exp = pd.pad(ex);

    return new Promise(function(resolve, reject) {
        lipishaClient.authorize_card_transaction(accNo, cno, 1000, ph, exp, fullname, "KENYA", "Nairobi", 6867, cvc, amnt, "KES", function(err, response) {
            if (err) {
                //Log failed authorisation in a lipisha authorisation log file      
                logger.failedLipishaLogger.info("FAILED AUTH");
                reject(err);
            } else if (response.status === "FAIL") {
                logger.failedLipishaLogger.info("FAILED AUTH: " + response.content.reason);
                reject(new Error(response.content.reason));
            } else {
                logger.successfulLipishaLogger.info("SUCCESSFUL AUTH: " + response.content.transaction_index);
                resolve(response);
            }
        });
    });
};

exports.completeLipisha = function(authResp) {
    var trans_index = authResp.content.transaction_index;
    var trans_ref = authResp.content.transaction_reference;

    return new Promise(function(resolve, reject) {
        lipishaClient.complete_card_transaction(trans_index, trans_ref, function(err, compRes) {
            if (err) {
                logger.failedLipishaLogger.info("FAILED PAYMENT COMPLETION");
                reject(err);
                //Log failed completion
            } else {
                var compRes_trans_index = compRes.content.transaction_index;
                var compRes_trans_ref = compRes.content.transaction_reference;
                if (compRes.status.status === "SUCCESS" && compRes_trans_index == trans_index) {
                    logger.successfulLipishaLogger.info("SUCCESSFUL PAYMENT COMPLETION: " + compRes_trans_index);
                    resolve(compRes);
                } else {
                    reject(new Error(compRes.status));
                }
            }
        })
    })
}

exports.processMobilePayment = function(mobileDetails) {
    var accno = mobileDetails.accountNumber;
    var mobileNo = mobileDetails.mobileNumber;
    var method = mobileDetails.method;
    var amount = mobileDetails.amount;
    var reference = mobileDetails.orderID;

    //account_number, mobile_number, method, amount, reference, callback
    return new Promise((resolve, reject) => {
        lipishaClient.request_money(accno, mobileNo, method, amount, reference, (err, response) => {
            if (err) {
                logger.failedLipishaLogger.info("FAILED MPESA PAYMENT");
                reject(err);
            } else {
                if (response.status.status === "SUCCESS") {
                    logger.successfulLipishaLogger.info("SUCCESSFUL MPESA PAYMENT: " + response.content.reference);
                    resolve(response);
                } else {
                    reject(new Error(response.status.status))
                }
            }
        })

    })
}