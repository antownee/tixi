var exports = module.exports = {};

const winston = require('winston');
const expressWinston = require('express-winston');

exports.filenames = [
    "admin-requests.log",
    "api-requests.log",
    "error.log",
    "transactions.log",
    "lipisha-auth.log",
    "lipisha-fail.log",
    "completed-orders.log",
    "emails-sent.log",
    "emails-unsent.log",
    "user-info.log",
    "ticket-info.log",
    "event-info.log"
]

//Log admin requests
exports.adminLogger = expressWinston.logger({
    transports: [
        new (winston.transports.File)({
            name: 'admin-requests',
            filename: 'log/admin-requests.log',
            level: 'info'
        })
    ]
})

//Log API requests
exports.apiLogger = expressWinston.logger({
    transports: [
        new (winston.transports.Console)({
            json: true,
            colorize: true
        }),
        new (winston.transports.File)({
            name: 'api-requests',
            filename: 'log/api-requests.log',
            level: 'info'
        })
    ]
})


//log errors
exports.errorLogger = expressWinston.errorLogger({
    transports: [
        new (winston.transports.File)({
            name: 'error-file',
            filename: 'log/error.log',
            level: 'error'
        })
    ]
})

//Log transaction flow
exports.transactionLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'transaction flow',
            filename: 'log/transactions.log',
            level: 'info'
        })
    ]
});


//Log successful lipisha authorizations
exports.successfulLipishaLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'lipisha',
            filename: 'log/lipisha-auth.log',
            level: 'info'
        })
    ]
});

//Log failed lipisha authorizations
exports.failedLipishaLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'lipisha',
            filename: 'log/lipisha-fail.log',
            level: 'error'
        })
    ]
});

//Log completed orders
exports.completedOrders = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'lipisha',
            filename: 'log/completed-orders.log',
            level: 'info'
        })
    ]
});


//Log sent emails
exports.sentEmailLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'emails-sent',
            filename: 'log/emails-sent.log',
            level: 'info'
        })
    ]
});


//Log unsent emails
exports.unsentEmailLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'emails-sent',
            filename: 'log/emails-unsent.log',
            level: 'info'
        })
    ]
});


//Log user information: Add, delete, updates
exports.userInfo = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'user-info',
            filename: 'log/user-info.log',
            level: 'info'
        })
    ]
});

//Log ticket information: Add, delete, updates
exports.ticketInfo = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'ticket-info',
            filename: 'log/ticket-info.log',
            level: 'info'
        })
    ]
});

//Log event information: Add, delete, updates
exports.eventInfo = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'event-info',
            filename: 'log/event-info.log',
            level: 'info'
        })
    ]
});