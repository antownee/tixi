'use strict';
const request = require('request');

const apiKey = '1a0514f7df1fd897f5bbf7fc0648ebb2550709cc0d9a2cb028f48adc7b6b23ea';
const atUrl = 'https://payments.africastalking.com/mobile/checkout/request';
const atUrl1 = 'https://api.africastalking.com/payment/mobile/checkout/request';

var reqBody = {
    "username": "antownee",
    "productName": "Tixi Ticket",
    "phoneNumber": "+254710460468",
    "currencyCode": "KES",
    "amount": 100
};

var options = {
    url: atUrl1,
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'apikey': apiKey
    },
    json: true,
    body: reqBody
};


request(options, (err, resp, body) => {
    if (err) { return console.log(err); }

    console.log(resp);
    console.log(body);
});