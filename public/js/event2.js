$(document).ready(function () {

    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        }

    });

    // Add smooth scrolling to all links
    $("#btn-buy").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });


    $('.sel').change(function () {
        var total = 0;
        var ticketCount = 0;
        var ticketArray = [];

        $('.ticketRow').each(function () {
            var tot = 0;
            //Obtain value
            var tcount = 0;
            tcount = $(this).find('.sel').val();
            //Obtain ticket price
            var price = $(this).find('.ticket-price').text();
            //Multiply
            tot = tcount * price;
            //
            $('.type-total', this).html(tot);
        });

        $('.ticketRow .type-total').each(function (i) {
            total += parseInt($(this).text());
        });

        $('#total-cost').html("KES " + total);

        $('.ticketRow .sel').each(function (i) {
            ticketCount += parseInt($(this).val());
        });

        if (ticketCount > 0) {
            $('#btn-toggle-buy').toggleClass("hide", false);
        } else if (ticketCount === 0) {
            $('#btn-toggle-buy').toggleClass("hide", true);
        }


    });



    function getTickets() {
        let tcks = [];
        
        $('.ticketRow').each(function (i) {
            var count = $(this).find('.sel').val();
            var id = $(this).find('.ticket-type-id').text();
            if (count !== 0) {
                for (var index = 0; index < count; index++) {
                    tcks.push({ id: id });
                }
            }
        });

        return tcks;
    }


    //Phone number fields
    $("#phoneNumberCard").intlTelInput({
        initialCountry: "ke",
        nationalMode: true,
        utilsScript: "js/utils.js" // just for formatting/placeholders etc
    });

    $("#phoneNumberMobile").intlTelInput({
        initialCountry: "ke",
        nationalMode: true,
        utilsScript: "js/utils.js" // just for formatting/placeholders etc
    });

    //Validation
    $('[data-numeric]').payment('restrictNumeric');
    $('.cc-number').payment('formatCardNumber');
    $('.cc-exp').payment('formatCardExpiry');
    $('.cc-cvc').payment('formatCardCVC');

    $.fn.toggleInputError = function (erred) {
        this.closest('.form-group').toggleClass('has-danger', erred);
        this.next().toggleClass('show', erred);
        this.next().toggleClass('hide', !erred);
        return this;
    };

    $('#card-payment-form').submit(function (e) {
        e.preventDefault();

        $('label.form-control-feedback').addClass('hide');

        var cardType = $.payment.cardType($('.cc-number').val());
        var cardNumberSuccess = $.payment.validateCardNumber($('.cc-number').val());
        var cardExpSuccess = $.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal'))
        var cardCVCSuccess = $.payment.validateCardCVC($('.cc-cvc').val(), cardType);
        $('.cc-number').toggleInputError(!cardNumberSuccess);
        $('.cc-exp').toggleInputError(!cardExpSuccess);
        $('.cc-cvc').toggleInputError(!cardCVCSuccess);
        $('.cc-brand').text(cardType);

        if (cardNumberSuccess && cardExpSuccess && cardCVCSuccess) {
            //console.log('YASS')#
            var totl = 0;
            $('.ticketRow .type-total').each(function (i) {
                totl += parseInt($(this).text());
            });

            var paymentForm = $('#payModal');
            paymentForm.find('.submit').prop('disabled', true);

            var fd = $('#card-payment-form').serialize();
            var tcks = getTickets();

            var d = fd + '&' + $.param({ "tickets": tcks }) + '&' + $.param({ "phNumber": $("#phoneNumberCard").intlTelInput("getNumber") });

            //Spinner
            $('#payModal').find('.modal-content').load('/public/api/loading');

            $.ajax({
                type: 'POST',
                url: '/public/api/create/card',
                data: d

            }).done(function (response) {
                //Load complete order page
                $('#payModal').find('.modal-content').load('/public/api/complete');
            }).fail(function (data) {
                $('#payModal').find('.modal-content').load('/public/api/incomplete');
            })
        } else {
            console.log('nah');
        }

    });




});