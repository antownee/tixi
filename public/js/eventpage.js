;
(function() {

    'use strict';



    // iPad and iPod detection	
    var isiPad = function() {
        return (navigator.platform.indexOf("iPad") != -1);
    };

    var isiPhone = function() {
        return (
            (navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPod") != -1)
        );
    };


    var fullHeight = function() {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function() {
            $('.js-fullheight').css('height', $(window).height());
        });

    };


    var burgerMenu = function() {

        $('.js-fh5co-nav-toggle').on('click', function(event) {
            event.preventDefault();
            var $this = $(this);
            if ($('body').hasClass('menu-show')) {
                $('body').removeClass('menu-show');
                $('#fh5co-main-nav > .js-fh5co-nav-toggle').removeClass('show');
            } else {
                $('body').addClass('menu-show');
                setTimeout(function() {
                    $('#fh5co-main-nav > .js-fh5co-nav-toggle').addClass('show');
                }, 900);
            }
        })
    };


    // Animations

    var contentWayPoint = function() {
        var i = 0;
        $('.animate-box').waypoint(function(direction) {

            if (direction === 'down' && !$(this.element).hasClass('animated')) {

                i++;

                $(this.element).addClass('item-animate');
                setTimeout(function() {

                    $('body .animate-box.item-animate').each(function(k) {
                        var el = $(this);
                        setTimeout(function() {
                            var effect = el.data('animate-effect');
                            if (effect === 'fadeIn') {
                                el.addClass('fadeIn animated');
                            } else {
                                el.addClass('fadeInUp animated');
                            }

                            el.removeClass('item-animate');
                        }, k * 200, 'easeInOutExpo');
                    });

                }, 100);

            }

        }, { offset: '85%' });
    };

    var sliderMain = function() {

        $('#fh5co-hero .flexslider').flexslider({
            animation: "fade",
            slideshowSpeed: 5000,
            directionNav: true,
            start: function() {
                setTimeout(function() {
                    $('.slider-text').removeClass('animated fadeInUp');
                    $('.flex-active-slide').find('.slider-text').addClass('animated fadeInUp');
                }, 500);
            },
            before: function() {
                setTimeout(function() {
                    $('.slider-text').removeClass('animated fadeInUp');
                    $('.flex-active-slide').find('.slider-text').addClass('animated fadeInUp');
                }, 500);
            }

        });

        $('#fh5co-hero .flexslider .slides > li').css('height', $(window).height());
        $(window).resize(function() {
            $('#fh5co-hero .flexslider .slides > li').css('height', $(window).height());
        });

    };

    var counter = function() {
        $('.js-counter').countTo({
            formatter: function(value, options) {
                return value.toFixed(options.decimals);
            },
        });
    };

    var counterWayPoint = function() {
        if ($('#counter-animate').length > 0) {
            $('#counter-animate').waypoint(function(direction) {

                if (direction === 'down' && !$(this.element).hasClass('animated')) {
                    setTimeout(counter, 400);
                    $(this.element).addClass('animated');
                }
            }, { offset: '90%' });
        }
    };


    var initTabs = function() {
        $("#payment-modal").on('shown.bs.modal', function(e) {
            var modal = $(this);
            modal.find('.modal-title').text("PAYMENT - KES " + $('#total-cost').text());
            var tab = e.relatedTarget.hash;
            $('.nav-tabs a[href="' + tab + '"]').tab('show');
        });
    }

    var initImageViewer = function() {
        $('.image-popup-vertical-fit').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }
        });
    }

    var calculate = function() {
        $('.sel').change(function() {
            var total = 0;
            var ticketCount = 0;

            $('.ticketRow').each(function() {
                var tot = 0;
                //Obtain value
                var tcount = 0;
                tcount = $(this).find('.sel').val();
                //Obtain ticket price
                var price = $(this).find('.ticket-price').text();
                //Multiply
                tot = tcount * price;
                //
                $('.type-total', this).html(tot);
            });

            $('.ticketRow .type-total').each(function(i) {
                total += parseInt($(this).text());
            });

            $('.ticketRow .sel').each(function(i) {
                ticketCount += parseInt($(this).val());
            });

            $('#total-cost').html(total);

            if (ticketCount > 0) {
                $('#pay-btn').removeClass('hide');
            } else {
                $('#pay-btn').addClass('hide');
            }
        });


    }



    
    function getTickets() {
        var tcks = [];
        $('.ticketRow').each(function(i) {
            var count = $(this).find('.sel').val();
            var price = $(this).find('.ticket-price').text();
            var type = $(this).find('.ticket-type').text();
            if (count !== 0) {
                for (var index = 0; index < count; index++) {
                    tcks.push({ ticketType: type, price: price });
                }
                return tcks;
            }
            return tcks;
        });
    }


    var validation = function() {
        $('[data-numeric]').payment('restrictNumeric');
        $('.cc-number').payment('formatCardNumber');
        $('.cc-exp').payment('formatCardExpiry');
        $('.cc-cvc').payment('formatCardCVC');

        $.fn.toggleInputError = function(erred) {
            this.parent('.form-group').toggleClass('has-error', erred);
            this.siblings('.control-label').toggleClass('show', erred);
            return this;
        };

        $('#card-payment-form').submit(function(e) {
            e.preventDefault();

            $('label.control-label').addClass('hide');

            var cardType = $.payment.cardType($('.cc-number').val());
            var cardNumberSuccess = $.payment.validateCardNumber($('.cc-number').val());
            var cardExpSuccess = $.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal'))
            var cardCVCSuccess = $.payment.validateCardCVC($('.cc-cvc').val(), cardType)
            $('.cc-number').toggleInputError(!cardNumberSuccess);
            $('.cc-exp').toggleInputError(!cardExpSuccess);
            $('.cc-cvc').toggleInputError(!cardCVCSuccess);
            $('.cc-brand').text(cardType);

            if (cardNumberSuccess && cardExpSuccess && cardCVCSuccess) {
                //console.log('YASS')#
                var totl = 0;
                $('.ticketRow .type-total').each(function(i) {
                    totl += parseInt($(this).text());
                });

                $('#card-payment-form').append($('<input type="hidden" name="amount">').val(totl));
                $('#mpesa-payment-form').append($('<input type="hidden" name="amount">').val(totl));



                var paymentForm = $('#card-payment-form');
                paymentForm.find('.submit').prop('disabled', true);

                var fd = $('#card-payment-form').serialize();
                getTickets();

                var d = fd + '&' + $.param({ "tickets": tcks }) + '&' + $.param({ "phNumber": $("#phoneNumberCard").intlTelInput("getNumber") });

                //Spinner
                $('#payment-modal').find('.modal-content').load('/api/orders/loading');

                $.ajax({
                    type: 'POST',
                    url: '/api/orders/create/card',
                    data: d

                }).done(function(response) {
                    //Load complete order page
                    $('#payment-modal').find('.modal-content').load('/api/orders/complete');
                    //alert("POSTED OK")
                }).fail(function(data) {
                    $('#payment-modal').find('.modal-content').load('/api/orders/incomplete');
                })
            } else {
                console.log('nah');
            }

        });

    }


    var mmform = function() {
        $('#mpesa-payment-form').submit(function(e) {
            e.preventDefault();

            $('label.control-label').addClass('vanish');

            if ('1' == '1') {
                //console.log('YASS')
                var paymentForm = $('#mpesa-payment-form');
                paymentForm.find('.submit').prop('disabled', true);

                var totl = 0;
                $('.ticketRow .type-total').each(function(i) {
                    totl += parseInt($(this).text());
                });

                $('#card-payment-form').append($('<input type="hidden" name="amount">').val(totl));
                $('#mpesa-payment-form').append($('<input type="hidden" name="amount">').val(totl));

                var fd = $('#mpesa-payment-form').serialize();
                getTickets();
                var d = fd + '&' + $.param({ "tickets": tcks }) + '&' + $.param({ "phNumber": $("#phoneNumberCard").intlTelInput("getNumber") });
                //Spinner
                $('#payment-modal').find('.modal-content').load('/api/orders/loading');

                $.ajax({
                    type: 'POST',
                    url: '/api/orders/create/mobile',
                    data: d

                }).done(function(response) {
                    //Load complete order page
                    $('#payment-modal').find('.modal-content').load('/api/orders/complete');
                    //alert("POSTED OK")
                }).fail(function(data) {
                    $('#payment-modal').find('.modal-content').load('/api/orders/incomplete');
                })
            } else {
                console.log('nah')
            }
        });

    };

    function mobileInputInit() {
        $("#phoneNumberCard").intlTelInput({
            initialCountry: "ke",
            nationalMode: true,
            utilsScript: "js/utils.js" // just for formatting/placeholders etc
        });

        $("#phoneNumberMobile").intlTelInput({
            initialCountry: "ke",
            nationalMode: true,
            utilsScript: "js/utils.js" // just for formatting/placeholders etc
        });
    }


    // Document on load.
    $(function() {
        fullHeight();
        burgerMenu();
        counter();
        sliderMain();
        contentWayPoint();
        counterWayPoint();
        initTabs();
        initImageViewer();
        calculate();
        validation();
        mmform();
        mobileInputInit();
    });


}());