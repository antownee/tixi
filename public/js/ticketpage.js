; (function () {

	'use strict';

	var mobileMenuOutsideClick = function () {

		$(document).click(function (e) {
			var container = $("#gtco-offcanvas, .js-gtco-nav-toggle");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				$('.js-gtco-nav-toggle').addClass('gtco-nav-white');

				if ($('body').hasClass('offcanvas')) {

					$('body').removeClass('offcanvas');
					$('.js-gtco-nav-toggle').removeClass('active');
				}
			}
		});

	};


	var offcanvasMenu = function () {

		$('#page').prepend('<div id="gtco-offcanvas" />');
		$('#page').prepend('<a href="#" class="js-gtco-nav-toggle gtco-nav-toggle gtco-nav-white"><i></i></a>');
		var clone1 = $('.menu-1 > ul').clone();
		$('#gtco-offcanvas').append(clone1);
		var clone2 = $('.menu-2 > ul').clone();
		$('#gtco-offcanvas').append(clone2);

		$('#gtco-offcanvas .has-dropdown').addClass('offcanvas-has-dropdown');
		$('#gtco-offcanvas')
			.find('li')
			.removeClass('has-dropdown');

		// Hover dropdown menu on mobile
		$('.offcanvas-has-dropdown').mouseenter(function () {
			var $this = $(this);

			$this
				.addClass('active')
				.find('ul')
				.slideDown(500, 'easeOutExpo');
		}).mouseleave(function () {

			var $this = $(this);
			$this
				.removeClass('active')
				.find('ul')
				.slideUp(500, 'easeOutExpo');
		});


		$(window).resize(function () {

			if ($('body').hasClass('offcanvas')) {

				$('body').removeClass('offcanvas');
				$('.js-gtco-nav-toggle').removeClass('active');

			}
		});
	};


	var burgerMenu = function () {

		$('body').on('click', '.js-gtco-nav-toggle', function (event) {
			var $this = $(this);


			if ($('body').hasClass('overflow offcanvas')) {
				$('body').removeClass('overflow offcanvas');
			} else {
				$('body').addClass('overflow offcanvas');
			}
			$this.toggleClass('active');
			event.preventDefault();

		});
	};



	var contentWayPoint = function () {
		var i = 0;

		// $('.gtco-section').waypoint( function( direction ) {


		$('.animate-box').waypoint(function (direction) {

			if (direction === 'down' && !$(this.element).hasClass('animated-fast')) {

				i++;

				$(this.element).addClass('item-animate');
				setTimeout(function () {

					$('body .animate-box.item-animate').each(function (k) {
						var el = $(this);
						setTimeout(function () {
							var effect = el.data('animate-effect');
							if (effect === 'fadeIn') {
								el.addClass('fadeIn animated-fast');
							} else if (effect === 'fadeInLeft') {
								el.addClass('fadeInLeft animated-fast');
							} else if (effect === 'fadeInRight') {
								el.addClass('fadeInRight animated-fast');
							} else {
								el.addClass('fadeInUp animated-fast');
							}

							el.removeClass('item-animate');
						}, k * 200, 'easeInOutExpo');
					});

				}, 100);

			}

		}, { offset: '85%' });
		// }, { offset: '90%'} );
	};


	var dropdown = function () {

		$('.has-dropdown').mouseenter(function () {

			var $this = $(this);
			$this
				.find('.dropdown')
				.css('display', 'block')
				.addClass('animated-fast fadeInUpMenu');

		}).mouseleave(function () {
			var $this = $(this);

			$this
				.find('.dropdown')
				.css('display', 'none')
				.removeClass('animated-fast fadeInUpMenu');
		});

	};

	var tabs = function () {

		// Auto adjust height
		$('.gtco-tab-content-wrap').css('height', 0);
		var autoHeight = function () {

			setTimeout(function () {

				var tabContentWrap = $('.gtco-tab-content-wrap'),
					tabHeight = $('.gtco-tab-nav').outerHeight(),
					formActiveHeight = $('.tab-content.active').outerHeight(),
					totalHeight = parseInt(tabHeight + formActiveHeight + 90);

				tabContentWrap.css('height', totalHeight);

				$(window).resize(function () {
					var tabContentWrap = $('.gtco-tab-content-wrap'),
						tabHeight = $('.gtco-tab-nav').outerHeight(),
						formActiveHeight = $('.tab-content.active').outerHeight(),
						totalHeight = parseInt(tabHeight + formActiveHeight + 90);

					tabContentWrap.css('height', totalHeight);
				});

			}, 100);

		};

		autoHeight();


		// Click tab menu
		$('.gtco-tab-nav a').on('click', function (event) {

			var $this = $(this),
				tab = $this.data('tab');

			$('.tab-content')
				.addClass('animated-fast fadeOutDown');

			$('.tab-content')
				.removeClass('active');

			$('.gtco-tab-nav li').removeClass('active');

			$this
				.closest('li')
				.addClass('active')

			$this
				.closest('.gtco-tabs')
				.find('.tab-content[data-tab-content="' + tab + '"]')
				.removeClass('animated-fast fadeOutDown')
				.addClass('animated-fast active fadeIn');


			autoHeight();
			event.preventDefault();

		});
	};


	var goToTop = function () {

		$('.js-gotop').on('click', function (event) {

			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500, 'easeInOutExpo');

			return false;
		});

		$(window).scroll(function () {

			var $win = $(window);
			if ($win.scrollTop() > 200) {
				$('.js-top').addClass('active');
			} else {
				$('.js-top').removeClass('active');
			}

		});

	};


	// Loading page
	var loaderPage = function () {
		$(".gtco-loader").fadeOut("slow");
	};


	var counterWayPoint = function () {
		if ($('#gtco-counter').length > 0) {
			$('#gtco-counter').waypoint(function (direction) {

				if (direction === 'down' && !$(this.element).hasClass('animated')) {
					setTimeout(counter, 400);
					$(this.element).addClass('animated');
				}
			}, { offset: '90%' });
		}
	};


	var loadSpinner = function () {
		$(document).ajaxSend(function () {
			//LOAD SPINNER
			$('#payment-modal').find('.modal-body').html('<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>');
		});
	}

	var initTabs = function () {
		$("#payment-modal").on('shown.bs.modal', function (e) {
			var modal = $(this);
			modal.find('.modal-title').text("PAYMENT - " + $('#total-cost').text());
			var tab = e.relatedTarget.hash;
			$('.nav-tabs a[href="' + tab + '"]').tab('show')
		});
	}

	var initImageViewer = function () {
		$('.image-popup-vertical-fit').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			}
		});
	}


	var calculate = function () {
		$('.sel').change(function () {
			var total = 0;
			var ticketCount = 0;

			$('.price-list .ticket-item').each(function () {
				var tot = 0;
				//Obtain value
				var tcount = 0;
				tcount = $(this).find('.sel').val();
				//Obtain ticket price
				var price = $(this).find('.ticket-price').text();
				//Multiply
				tot = tcount * price;
				//
				$('.type-total', this).html(tot);
				$('.type-count', this).html(tcount);
			});

			$('.price-list .type-total').each(function (i) {
				total += parseInt($(this).text());
			});

			$('.price-list .type-count').each(function (i) {
				ticketCount += parseInt($(this).text());
			});

			$('#total-cost').html("KES " + total);

			if (ticketCount > 0) {
				$('#pay-btn').removeClass('hide');
			} else {
				$('#pay-btn').addClass('hide');
			}
		});


	}



	var tcks = [];
	function getTickets() {
		$('.price-list .ticket-item').each(function (i) {
			var count = $(this).find('.sel').val();
			var price = $(this).find('.ticket-price').text();
			var type = $(this).find('.ticket-type').text();
			if (count !== 0) {
				for (var index = 0; index < count; index++) {
					tcks.push({ ticketType: type, price: price });
				}
				return;
			}
			return;
		});
	}


	var validation = function () {
		$('[data-numeric]').payment('restrictNumeric');
		$('.cc-number').payment('formatCardNumber');
		$('.cc-exp').payment('formatCardExpiry');
		$('.cc-cvc').payment('formatCardCVC');

		$.fn.toggleInputError = function (erred) {
			this.parent('.form-group').toggleClass('has-error', erred);
			this.siblings('.control-label').toggleClass('show', erred);
			return this;
		};

		$('#card-payment-form').submit(function (e) {
			e.preventDefault();

			$('label.control-label').addClass('vanish');

			var cardType = $.payment.cardType($('.cc-number').val());
			var cardNumberSuccess = $.payment.validateCardNumber($('.cc-number').val());
			var cardExpSuccess = $.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal'))
			var cardCVCSuccess = $.payment.validateCardCVC($('.cc-cvc').val(), cardType)
			$('.cc-number').toggleInputError(!cardNumberSuccess);
			$('.cc-exp').toggleInputError(!cardExpSuccess);
			$('.cc-cvc').toggleInputError(!cardCVCSuccess);
			$('.cc-brand').text(cardType);

			if (cardNumberSuccess && cardExpSuccess && cardCVCSuccess) {
				//console.log('YASS')#
				var totl = 0;
				$('.price-list .type-total').each(function (i) {
					totl += parseInt($(this).text());
				});

				$('#card-payment-form').append($('<input type="hidden" name="amount">').val(totl));
				$('#mpesa-payment-form').append($('<input type="hidden" name="amount">').val(totl));


				var paymentForm = $('#card-payment-form');
				paymentForm.find('.submit').prop('disabled', true);

				var fd = $('#card-payment-form').serialize();
				getTickets();
				var d = fd + '&' + $.param({ "tickets": tcks });

				//Spinner
				$('#payment-modal').find('.modal-content').load('/api/orders/loading');

				$.ajax({
					type: 'POST',
					url: '/api/orders/create/card',
					data: d

				}).done(function (response) {
					//Load complete order page
					$('#payment-modal').find('.modal-content').load('/api/orders/complete');
					//alert("POSTED OK")
				}).fail(function (data) {
					$('#payment-modal').find('.modal-content').load('/api/orders/incomplete');
				})
			}
			else {
				console.log('nah');
			}

		});

	}


	var mmform = function () {
		$('#mpesa-payment-form').submit(function (e) {
			e.preventDefault();

			$('label.control-label').addClass('vanish');

			if ('1' == '1') {
				//console.log('YASS')
				var paymentForm = $('#mpesa-payment-form');
				paymentForm.find('.submit').prop('disabled', true);

				var totl = 0;
				$('.price-list .type-total').each(function (i) {
					totl += parseInt($(this).text());
				});

				$('#card-payment-form').append($('<input type="hidden" name="amount">').val(totl));
				$('#mpesa-payment-form').append($('<input type="hidden" name="amount">').val(totl));

				var fd = $('#mpesa-payment-form').serialize();
				getTickets();
				var d = fd + '&' + $.param({ "tickets": tcks });
				//Spinner
				$('#payment-modal').find('.modal-content').load('/api/orders/loading');
				
				$.ajax({
					type: 'POST',
					url: '/api/orders/create/mobile',
					data: d

				}).done(function (response) {
					//Load complete order page
					$('#payment-modal').find('.modal-content').load('/api/orders/complete');
					//alert("POSTED OK")
				}).fail(function (data) {
					$('#payment-modal').find('.modal-content').load('/api/orders/incomplete');
				})
			}
			else {
				console.log('nah')
			}
		});

	}


	$(function () {
		mobileMenuOutsideClick();
		offcanvasMenu();
		burgerMenu();
		contentWayPoint();
		dropdown();
		tabs();
		goToTop();
		//loaderPage();
		calculate();
		validation();
		initTabs();
		initImageViewer(); //Popup
		//loadSpinner();
		mmform();
	});
} ());