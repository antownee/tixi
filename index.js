'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
require('dotenv').config();
const app = express();
const fs = require('fs');
const pug = require('pug');
const path = require('path');

//Connect to db
const port = process.env.PORT || 3000;
const mongoUri = process.env.MONGODB_LOCAL;
require('./config/db').connect(port, mongoUri);

/**
 * Cors
 */
app.use(cors());


/**
 * Passport
 */
const localSignupStrategy = require('./passport/local-signup');
const localLoginStrategy = require('./passport/local-login');
passport.use('local-signup', localSignupStrategy);
passport.use('local-login', localLoginStrategy);


/**
 * Middleware
 */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());
//Serve up public files
app.use(express.static(__dirname + "/public"));
//Ability to serve pug files
//app.set('views', './views');
//app.set('view engine',"pug"); 


/**
 * Routers
 */

let staticFilesRouter = require("./routers/public/static-files");
app.use('/', staticFilesRouter);
//Serve event page list and event page.
let eventRouter = require("./routers/public/events");
app.use('/events', eventRouter);
//Posting event order information
let publicApiRouter = require("./routers/api/public-api");
app.use('/public/api', publicApiRouter);

//PRIVATE//
const authcheckMiddleware = require('./middleware/auth-check.js');
const authLoginSignup = require('./routers/auth/auth');
let apiRouter = require('./routers/api/api');
app.use('/api', [authcheckMiddleware, apiRouter]);
app.use('/auth', authLoginSignup);

/**
 * Error Handling
 */
if (process.env.NODE_ENV === 'development') {
    require('express-debug')(app, {});
    app.use(function (err, req, res, next) {
        console.error(err.stack);
        throw err;
        //Error page
    });
}
// Production
if (process.env.NODE_ENV === 'production') {
    app.use(function (err, req, res, next) {
        console.error(err.stack);
        res.sendStatus(err.status || 500);
    });
}

/**
 * Initial setup of files
 */
//Log folder creation
let logdir = './log';
if (!fs.existsSync(logdir)) {
    fs.mkdirSync(logdir);
}


app.listen(port, () => {
    console.log(`Tixi running on port ${port}`);
});