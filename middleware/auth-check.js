'use strict';
const jwt = require('jsonwebtoken');
const config = require('../config/config');
const user = require('../models/user');

module.exports = (req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(401).end();
    }
    const token = req.headers.authorization.split(' ')[1];

    jwt.verify(token, config.jwt, (err, decoded) => {
        if (err) { return res.status(401).end(); } //Unauthorised

        const userId = decoded.data.sub.id;

        // check if a user exists
        user.findById(userId, (userErr, usr) => {
            if (userErr || !usr) {
                return res.status(401).end();
            }

            return next();
        });
    });

};