'use strict';
const mongoose = require('mongoose');

var exports = module.exports = {};

const port = process.env.PORT || 8000;
const mongoUri = process.env.MONGODB_LOCAL;

exports.connect = (port, mongoUri) => {

    mongoose.Promise = global.Promise;
    mongoose.connect(mongoUri);

    mongoose.connection.on('connected', () => {
        console.log('MongoDB connection established. ' + mongoUri);
    });

    mongoose.connection.on('error', (err) => {
        console.log(`MongoDB Connection Error. ${err} Please make sure that MongoDB is running.`);
        process.exit(1);
    });

}

