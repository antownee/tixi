'use strict';
const express = require('express');
const app = express();
const dotenv = require('dotenv').config();
const mongoose = require('mongoose');
const session = require('express-session');
const flash = require('express-flash');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const helmet = require('helmet');
const async = require('async');
const requireIp = require('request-ip');
const passport = require('passport');
const MongoStore = require('connect-mongo')(session);
const lusca = require('lusca');
const multer = require('multer');
const mongoSanitize = require('express-mongo-sanitize');
const fs = require('fs');
const mime = require('mime');

const eventCustomiseRouter = require('./routers/admin/event/customise');
const eventOrderApiRouter = require('./routers/api/orders');

//Load passport config
require('./utils/passport');

//Load env files
//dotenv.load()

//Db setup
const port = process.env.PORT || 3000;
const mongoUri = process.env.MONGODB_LOCAL || process.env.MONGOLAB_URI;
mongoose.Promise = global.Promise;
mongoose.connect(mongoUri);
mongoose.connection.on('connected', function() {
    console.log('MongoDB connection established. ' + mongoUri);
});

mongoose.connection.on('error', function() {
    console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
    process.exit(1);
});


//Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(mongoSanitize());
app.use(express.static("public")); //Serve up public files
app.use(express.static("node_modules/patternfly/dist"));
app.use(express.static("node_modules/patternfly/node_modules"));
app.use(validator());
app.use(requireIp.mw());
app.use(session({
    secret: process.env.SESSION_SECRET,
    store: new MongoStore({
        url: mongoUri,
        autoReconnect: true
    }),
    resave: true,
    saveUninitialized: true,
    maxAge: 60000
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

//Picture uploading form routers
var fpath = null;
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'public/uploads/posters');
    },
    filename: function(req, file, cb) {
        fpath = `${file.fieldname}-${Date.now()}.${mime.extension(file.mimetype)}`;
        req.session.fpath = fpath;
        cb(null, fpath);
    }
});
const upload = multer({ storage: storage });
app.post('/admin/event/customise/first', upload.single('eventPoster'), eventCustomiseRouter.uploadImage);
app.post('/admin/event/customise', upload.single('eventPoster'), eventCustomiseRouter.uploadImage);


//SECURITY Middleware
app.use(helmet());
app.disable('x-powered-by');
app.use(lusca({
    csrf: true,
    // csp: { 
    //   policy: {
    //     'default-src': '\'self\'',
    //     'img-src': '*'
    //   }
    // },
    xframe: 'SAMEORIGIN',
    p3p: 'ABCDEF',
    hsts: { maxAge: 31536000, includeSubDomains: true, preload: true },
    xssProtection: true,
    nosniff: true
}));

//Create log folder
var logdir = './log';

if (!fs.existsSync(logdir)) {
    fs.mkdirSync(logdir);
}

//View Engine
app.set('view-engine', 'pug');


//PUBLIC ROUTER
var publicRouter = require("./routers/public/public");
app.use(publicRouter);

//PUBLIC ROUTER
var eventRouter = require("./routers/public/events");
app.use('/events', eventRouter);
//AUTH
var authRouter = require("./routers/auth/auth");
app.use(authRouter);

//ODIN ROUTER
var odinRouter = require("./routers/odin/odin");
app.use("/odin", odinRouter);



////AUTHENTICATE THE ADMIN ROUTE !
app.use(function(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
})

//ADMIN ROUTER
var adminRouter = require("./routers/admin/admin");
app.use("/admin", adminRouter);


///ERROR HANDLING
// Development
if (process.env.NODE_ENV === 'development') {
    require('express-debug')(app, {});
    app.use(function(err, req, res, next) {
        console.error(err.stack);
        throw err;
    });
}
// Production
if (process.env.NODE_ENV === 'production') {
    app.use(function(err, req, res, next) {
        console.error(err.stack);
        res.sendStatus(err.status || 500);
    });
}


//Start it up!
app.listen(port, function() {
    console.log("Tixi listening on port " + port + ": %s mode", process.env.NODE_ENV);
});