'use strict';
const mongoose = require('mongoose');
const util = require('util');
const bodyParser = require('body-parser');
const express = require("express");
const passport = require('passport');
const user = require('../../models/user');
const Event = require('../../models/event');
const async = require('async');
const crypto = require('crypto');
const mailer = require("../../utils/mailer")
const validator = require('validator');


//Used to handle 
//1. Login get and post -- DONE
//2. Signup get and post -- DONE
//3. Forgot password get and post -- DONE
//4. Reset password post and get
//4. Logout -- DONE

//Define before various routers
const router = express.Router();

function validateSignupForm(payload) {
    const errors = {};
    let isFormValid = true;
    let message = '';

    if (!payload || typeof payload.email !== 'string' || !validator.isEmail(payload.email)) {
        isFormValid = false;
        errors.email = 'Please provide a correct email address.';
    }

    if (!payload || typeof payload.password !== 'string' || payload.password.trim().length < 8) {
        isFormValid = false;
        errors.password = 'Password must have at least 8 characters.';
    }

    if (!payload || typeof payload.fname !== 'string' || payload.fname.trim().length === 0) {
        isFormValid = false;
        errors.name = 'Please provide your first name.';
    }

    if (!payload || typeof payload.lname !== 'string' || payload.lname.trim().length === 0) {
        isFormValid = false;
        errors.name = 'Please provide your last name.';
    }

    if (!isFormValid) {
        message = 'Check the form for errors.';
    }

    return {
        success: isFormValid,
        message,
        errors
    };
}

function validateLoginForm(payload) {
    const errors = {};
    let isFormValid = true;
    let message = '';

    if (!payload || typeof payload.email !== 'string' || payload.email.trim().length === 0) {
        isFormValid = false;
        errors.email = 'Please provide your email address.';
    }

    if (!payload || typeof payload.password !== 'string' || payload.password.trim().length === 0) {
        isFormValid = false;
        errors.password = 'Please provide your password.';
    }

    if (!isFormValid) {
        message = 'Check the form for errors.';
    }

    return {
        success: isFormValid,
        message,
        errors
    };
}

// router.get('/login', function (req, res, next) {
//     if (req.user) {
//         return res.redirect('/admin');
//     }
//     res.render('account/login.pug');
// })

router.post('/login', (req, res, next) => {
    const validationResult = validateLoginForm(req.body);
    if (!validationResult.success) {
        return res.status(400).json({
            success: false,
            message: validationResult.message,
            errors: validationResult.errors
        });
    }


    return passport.authenticate('local-login', (err, token, userData) => {
        if (err) {
            if (err.name === 'IncorrectCredentialsError') {
                return res.status(400).json({
                    success: false,
                    message: err.message
                });
            }

            return res.status(400).json({
                success: false,
                message: err.message || 'Could not process the form.'
            });
        }


        return res.json({
            success: true,
            message: 'You have successfully logged in!',
            token
        });
    })(req, res, next);
});

router.get('/signup', (req, res, next) => {
    if (req.user) {
        return res.redirect('/admin');
    }
    res.render('account/signup.pug');
})

router.post('/signup', (req, res, next) => {
    const validationResult = validateSignupForm(req.body);
    if (!validationResult.success) {
        return res.status(400).json({
            success: false,
            message: validationResult.message,
            errors: validationResult.errors
        });
    }


    return passport.authenticate('local-signup', (err) => {
        if (err) {
            if (err.name === 'MongoError' && err.code === 11000) {
                // the 11000 Mongo code is for a duplication email error
                // the 409 HTTP status code is for conflict error
                return res.status(409).json({
                    success: false,
                    message: 'Check the form for errors.',
                    errors: {
                        email: 'This email is already taken.'
                    }
                });
            }

            return res.status(400).json({
                success: false,
                message: 'Could not process the form.'
            });
        }

        return res.status(200).json({
            success: true,
            message: 'You have successfully signed up! Now you should be able to log in.'
        });
    })(req, res, next);
});

router.get('/logout', (req, res, next) => {
    req.logOut();
    req.session.destroy(function (err) {
        // cannot access session here
        if (err) { return next(err) }
        res.redirect('/login')
    });
});


router.get('/forgot', (req, res, next) => {
    //Route to forgot page
    if (req.isAuthenticated()) {
        return res.redirect('/admin');
    }
    res.render('account/forgot.pug');
});


router.post('/forgot', (req, res, next) => {
    //Generate a random token and sends email with that tooken to reset account
    req.assert('email', 'Please enter a valid email').isEmail();
    req.sanitize('email').normalizeEmail({ remove_dots: true });

    var errors = req.validationErrors();

    if (errors) {
        req.flash('errors', errors);
        return res.redirect('/forgot');
    }

    //Generate random token and email
    async.waterfall([
        function (done) {
            //Generate random key
            crypto.randomBytes(16, (err, buf) => {
                const token = buf.toString('hex');
                done(null, token);
            })
        },
        function (token, done) {
            //Save token to db
            //First find user in db, then store
            User.findOne({ email: req.body.email }, (err, user) => {
                if (err) { return next(err) }
                if (!user) {
                    req.flash('errors', { msg: `User ${req.body.email} does not exist in our system.` });
                    return res.redirect('/forgot');
                }

                user.passwordResetToken = token;
                user.passwordResetExpires = Date.now() + 3600000 //In an hour

                user.save((err) => {
                    done(err, token, user);
                })
            })
        },
        function (token, user, done) {
            //Send email with token
            var mailOptions = {
                from: '"Tixi" <mailgun@sandboxce7c80c49d684d6fb34176afb7305bfc.mailgun.org>',
                to: 'muisyoanthony@gmail.com',
                subject: 'Tixi: Password reset',
                text: `You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n
                Please click on the following link, or paste this into your browser to complete the process:\n\n
                http://${req.headers.host}/reset/${token}\n\n
                If you did not request this, please ignore this email and your password will remain unchanged.\n`
            }
            mailer.sendMail(mailOptions, "PASSWORD RESET SENT", next);
            req.flash('info', { msg: `An email has been sent to ${req.body.email} for further details.` })
            done(null);
        }
    ], (err) => {
        if (err) { next(err) };
        res.redirect('/forgot');
    })

});

router.get('/reset/:token', (req, res, next) => {
    if (req.isAuthenticated()) {
        return res.redirect('/admin')
    }

    user.findOne({ passwordResetToken: req.params.token })
        .where('passwordResetExpires').gt(Date.now())
        .exec((err, usr) => {
            if (err) { return next(err) };
            if (!usr) {
                req.flash('errors', { msg: "The password reset token is invalid or has expired" });
                return res.redirect('/forgot');
            }
            res.render('account/reset.pug');
        });
});

router.post('/reset/:token', (req, res, next) => {
    req.assert('login__password', 'Password must be at least 7 characters long').len(7);
    req.assert('login__passwordconfirm', 'Your passwords do not match').equals(req.body.login__password);

    var errors = req.validationErrors();

    if (errors) {
        req.flash('errors', errors);
        return res.redirect('back');
    }

    async.waterfall([
        function (done) {
            //Update passowrd after checking validity of token
            user.findOne({ passwordResetToken: req.params.token })
                .where('passwordResetExpires').gt(Date.now())
                .exec((err, usr) => {
                    if (err) { return next(err); }
                    if (!user) {
                        req.flash('errors', { msg: "The password reset token is invalid or has expired" });
                        return res.redirect('back');
                    }

                    usr.comparePassword(req.body.login__password, (err, isMatch) => {
                        if (err) { return next(err); }
                        if (isMatch) {
                            req.flash('errors', { msg: "Your new password cannot match your old password." });
                            return res.redirect('back');
                        }

                        usr.password = req.body.login__password;
                        usr.passwordResetToken = undefined;
                        usr.passwordResetExpires = undefined;

                        //Save and log in the user
                        usr.save((err) => {
                            if (err) { next(err) };
                            req.logIn(user, (error) => {
                                done(error, user)
                            });
                        });
                    });
                });
        },
        function (user, done) {
            //Send email to complete password changing
            var mailOptions = {
                from: '"Tixi" <mailgun@sandboxce7c80c49d684d6fb34176afb7305bfc.mailgun.org>',
                to: 'muisyoanthony@gmail.com',
                subject: 'Tixi: Your password has been changed',
                text: `Hi ${user.name.first},\n\nThis is a confirmation that your password has been changed.\n
                Should you have any problem please don't hesitate to contact us via 0710460468 or sending an email to help@tixi.co.ke.\n\n
                `
            }
            mailer.sendMail(mailOptions, "PASSWORD CHANGE CONFIRMATION SENT", next);
            req.flash('success', { msg: `Your password has been changed.` });
            done(null);

        }
    ], (err) => {
        if (err) { next(err); }
        return res.redirect('/login')
    });
});

module.exports = router;