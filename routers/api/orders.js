'use strict';
const express = require("express");
const async = require("async");
const pug = require('pug');
const pdf = require('html-pdf');
const _ = require("lodash");
const excel = require('node-excel-export');
const datatablesQuery = require('datatables-query')
const bodyParser = require('body-parser');
var dateFormat = require('dateformat');
const numeral = require('numeral');
const utls = require("../../utils/utils.js");



const order = require("../../models/order");
const event = require("../../models/event");

//Compile ticket template
const compiledTicket = pug.compileFile(__dirname + "/../../views/ticket.pug");

var router = express.Router();
module.exports = router;

router.use(bodyParser.urlencoded({ extended: true }));

router.get("/:id", function (req, res, next) {
    order.find({ eventID: req.params.id }, function (err, data) {
        if (err) { res.send(404, new Error(err.message)); }
        if (data.length === 0) {
            res.send(404, "Nothing to show");
        }

        const id = req.params.id;
        async.series([
            function (cb) {
                //Total sales
                getTotalOrderSales(id, cb);
            },
            function (cb) {
                //Total tickets sold
                getTotalTicketsSold(id, cb);
            },
            function (cb) {
                //Total sales
                getNumberOfOrders(id, cb);
            }
        ], function (err, results) {
            if (err) { return res.send(404, new Error(err.message)); }
            return res.json({
                totalOrderSales: numeral(results[0].totalAmount).format("0,0"),
                numberOfTickets: numeral(results[1]).format("0,0"),
                numberOfOrders: numeral(results[2]).format("0,0"),
                orders: data
            });
        });
    });
});

router.post("/find", (req, res, next) => {
    var params = req.body;
    var query = datatablesQuery(order);

    query.run(params).then(function (unfilteredData) {
        //draw
        //recordsFiltered
        //recordsTotal
        const filteredData = _.filter(unfilteredData.data, (d) => {
            return d.eventID === req.session.eventalias;
        });
        res.json({
            data: filteredData,
            draw: unfilteredData.draw,
            recordsFiltered: unfilteredData.recordsFiltered,
            recordsTotal: unfilteredData.recordsTotal
        });
    }, function (err) {
        res.status(500).json(err);
    });
});

router.get("/modal/:id", function (req, res, next) {
    order.findOne({ orderID: req.params.id }, function (err, data) {
        if (err) next(err);
        if (!data) {
            return res.redirect("/admin/event/orders");
        }
        return res.render("event/ordermodal.pug", { order: data });
    })
})

router.get("/print/:id", (req, res, next) => {
    //Get ticket.
    var tickID = req.params.id;
    //Get ticket from db
    const evid = "1001";
    async.waterfall([
        function (cb) {
            //Get ticket and pass it on

            order.aggregate([
                { $match: { eventID: evid } },
                { $project: { tickets: 1 } }, /* select the tickets field as something we want to "send" to the next command in the chain */
                { $unwind: '$tickets' }, /* this converts arrays into unique documents for counting */
                {
                    $group: { /* execute 'grouping' */
                        _id: '$tickets',
                        /* using the 'token' value as the _id */
                    }
                },
                { $match: { '_id.ticketID': tickID } }
            ], (err, ticket) => {
                if (err) { return cb(err) };

                return cb(null, ticket[0]._id)
            })
        },
        function (ticket, cb) {
            event.findOne({ eventaliasID: evid }, (err, evnt) => {
                if (err) { return cb(err); }
                return cb(null, ticket, evnt);
            })
        },
        function (ticket, event, cb) {
            getPdf(ticket, event, cb);
        }
    ], function (err, pdfData) {
        //Results  = PDF
        res.writeHead(200, {
            'Content-Type': 'application/pdf',
            'Content-Disposition': 'attachment; filename=' + pdfData.filename,
            'Content-Length': pdfData.content.length
        });
        res.end(pdfData.content);

    })
})

router.get("/list/xls", (req, res, next) => {
    async.waterfall([
        function (cb) {
            order.find({ eventID: req.session.eventalias }, (err, results) => {
                if (err) { return cb(err) };

                var orders = _.chain(results)
                    .map((itm) => {
                        var dt = new Date(itm.datePurchased)
                        var eventDate = dateFormat(dt, "dddd, mmmm dS, yyyy, h:MM TT");
                        var tcks = itm.tickets;

                        for (var index = 0; index < tcks.length; index++) {
                            var ticket = tcks[index];
                            return {
                                name: itm.name.first,
                                email: itm.email,
                                amount: itm.amount,
                                ticketID: ticket.ticketID,
                                ticketType: ticket.ticketType,
                                date: itm.datePurchased
                            };
                        }
                    }).value();

                return cb(null, orders)
            });
        },
        function (orders, cb) {
            getOrdersXLS(orders, cb);
        }
    ], function (err, xlsdata) {
        if (err) { next(err); }
        //Return the xls
        var ts = new Date().getTime().toString();

        res.writeHead(200, {
            'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition': 'attachment; filename=' + "tixi_eventlist_" + ts + ".xlsx",
            'Content-Length': xlsdata.length
        });
        res.end(xlsdata);
    })

});


function getTotalOrderSales(id, cb) {
    order.aggregate([
        { $match: { eventID: id } },
        {
            $group: {
                _id: null,
                amount: { $sum: "$amount" }
            }
        }
    ], (err, res) => {
        if (err) { return cb(err) };
        const total = _.first(res);
        if (total == undefined) {
            return cb(null, { totalAmount: 0 });
        }
        return cb(null, { totalAmount: total.amount });
    })
}

function getTotalTicketsSold(id, cb) {
    order.aggregate([
        { $match: { eventID: id } },
        { $project: { tickets: 1 } }, /* select the tickets field as something we want to "send" to the next command in the chain */
        { $unwind: '$tickets' }, /* this converts arrays into unique documents for counting */
        {
            $group: { /* execute 'grouping' */
                _id: '$tickets',
                /* using the 'token' value as the _id */
            }
        }
    ], (err, totalTickets) => {
        if (err) { return cb(err) };

        return cb(null, totalTickets.length)
    })
}

function getNumberOfOrders(id, cb) {
    order.count({ eventID: id }, (err, count) => {
        if (err) { return cb(err) }

        return cb(null, count);
    })
}

function getPdf(ticket, evnt, cb) {
    var eventDate = utls.getDate(evnt.startDate.Date, evnt.startDate.Time)
    
    var ticketHtml = compiledTicket({
        ticketID: ticket.ticketID,
        ticketType: ticket.ticketType,
        price: ticket.price,
        evntName: evnt.eventName,
        location: evnt.eventLocation,
        date: eventDate
    })

    const port = process.env.PORT;
    pdf.create(ticketHtml, {
        "format": "Legal",
        "base": 'http://localhost:' + port,
        "orientation": "landscape",
    }).toBuffer(function (err, pdf) {
        if (err) { return cb(err); }
        var b = new Buffer(pdf);
        var attachment = {
            filename: ticket.ticketID + '.pdf',
            content: b
        };
        return cb(null, attachment)
    })
}

function getOrdersXLS(orders, cb) {
    var styles = {
        headerDark: {
            fill: {
                fgColor: {
                    rgb: 'FF000000'
                }
            },
            font: {
                color: {
                    rgb: 'FFFFFFFF'
                },
                sz: 14,
                bold: true
            }
        }
    };


    //Here you specify the export structure
    // orderID , name.first, email, amount, tickets.length
    //Header showing event name
    var specification = {
        name: {
            displayName: 'Name',
            headerStyle: styles.headerDark,
            width: '20' // <- width in chars (when the number is passed as string)
        },
        email: {
            displayName: 'Email',
            headerStyle: styles.headerDark,
            width: '30' // <- width in chars (when the number is passed as string)
        },
        amount: {
            displayName: 'Amount',
            headerStyle: styles.headerDark,
            width: '10' // <- width in chars (when the number is passed as string)
        },
        ticketID: {
            displayName: 'TicketID',
            headerStyle: styles.headerDark,
            width: '20' // <- width in chars (when the number is passed as string)
        },
        ticketType: {
            displayName: 'Type',
            headerStyle: styles.headerDark,
            width: '10' // <- width in chars (when the number is passed as string)
        },
        date: {
            displayName: 'Date',
            headerStyle: styles.headerDark,
            width: '20' // <- width in chars (when the number is passed as string)
        }
    }

    // The data set should have the following shape (Array of Objects)
    // The order of the keys is irrelevant, it is also irrelevant if the
    // dataset contains more fields as the report is build based on the
    // specification provided above. But you should have all the fields
    // that are listed in the report specification
    // var dataset = [
    //     { customer_name: 'IBM', status_id: 1, note: 'some note', misc: 'not shown' },
    //     { customer_name: 'HP', status_id: 0, note: 'some note' },
    //     { customer_name: 'MS', status_id: 0, note: 'some note', misc: 'not shown' }
    // ]

    var dataset = orders;

    var report = excel.buildExport(
        [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
            {
                name: 'Sheet name', // <- Raw heading array (optional)
                specification: specification, // <- Report specification
                data: dataset // <-- Report data
            }
        ]
    );

    cb(null, report);
}