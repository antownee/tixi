'use strict';
var bodyParser = require('body-parser');
var express = require("express");
const authcheckMiddleware = require('../../middleware/auth-check.js');

//Define before various routers
var router = express.Router();


//Some form of validation and have it in a middleware
router.use(bodyParser.urlencoded({ extended: true }))

//Dashboard
var dashboardRouter = require("./dashboard");
router.use("/dashboard", dashboardRouter);


//Users
var usersRouter = require("./users");
router.use("/users", usersRouter);

//Orders
var orderRouter = require("./orders");
router.use("/orders", orderRouter);

//Tickets
var ticketRouter = require("./ticketType");
router.use("/tickets", ticketRouter);

//Customise
var customiseRouter = require("./customise");
router.use("/customise", customiseRouter);

module.exports = router;