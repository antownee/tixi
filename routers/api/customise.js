'use strict';
const express = require("express");
const event = require("../../models/event");
const sanitizehtml = require('sanitize-html');
const shortid = require('shortid');
const cheerio = require('cheerio');
const nodeurl = require('url');
const logger = require('../../utils/logger.js');
const multer = require('multer');
const fs = require('fs');
const mime = require('mime');

const posterDir = 'public/uploads/posters';
const posterPathDir = 'uploads/posters';

var router = express.Router();
module.exports = router;

function geneventID() {
    shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
    return shortid.generate();
}

//Picture uploading form routers


router.get("/first", function (req, res, next) {
    return res.render('event/customise.pug', {
        event: {
            eventDescription: ""
        },
        firstTime: true
    });
});


router.get("/:id", function (req, res, next) {
    let id = req.params.id;
    event.findOne({ eventaliasID: id }, (err, ev) => {
        if (err) { return next(err); }
        if (!ev) { return res.status(404); }

        return res.status(200).json(ev);
    });
});



const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, posterDir);
    },
    filename: function (req, file, cb) {
        let fpath = `${file.fieldname}-${Date.now()}.${mime.extension(file.mimetype)}`;
        cb(null, fpath);
    }
});
const upload = multer({ storage: storage });


router.post("/add", upload.single("img"), (req, res, next) => {
    if (req.body.isEdit === "true") {
        //UPDATE
        return updateEvent(req, res, next);

    } else {
        //POST
        return postEvent(req, res, next);
    }
});


function postEvent(req, res, next) {
    //SANITIZE

    var cleanDescription = sanitizehtml(req.body.eventDescription, {
        allowedTags: sanitizehtml.defaults.allowedTags.concat(['iframe']),
        allowedAttributes: {
            a: ['href', 'name', 'target'],
            iframe: ['allowfullscreen', 'width', 'height', 'src', 'frameborder', 'allowfullscreen']
        }
    });

    var $ = cheerio.load(cleanDescription);
    $('iframe').each(function (i, elem) {
        var src = $(this).attr('src');
        //If src isn't youtube, then remove iframe
        const refReq = nodeurl.parse(src, true);
        if (refReq.host !== "www.youtube.com") {
            //Remove element
            $(this).remove();
        }
    });

    const evname = req.body.eventName;
    const evVenue = req.body.eventLocation;
    const evDescriptionHTML = cleanDescription;
    const startDate = {
        Date: JSON.parse(req.body.startDate).Date,
        Time: JSON.parse(req.body.startDate).Time
    };
    const endDate = {
        Date: JSON.parse(req.body.endDate).Date,
        Time: JSON.parse(req.body.endDate).Time
    };

    let posterPath = req.file == null ? "" : `${posterPathDir}/${req.file.filename}`;

    const ev = new event({
        eventID: geneventID(),
        userID: JSON.parse(req.body.user).id,
        eventName: evname,
        eventLocation: evVenue,
        eventDescription: evDescriptionHTML,
        startDate: startDate,
        endDate: endDate,
        posterPath: posterPath
    });

    ev.save(function (err, ev) {
        if (err) { return next(err); }

        let msg = `Event ${ev.eventName} : ${ev.eventID} has been added.`;
        logger.eventInfo.info(msg);
        return res.status(200).json({
            event: ev,
            msg: msg
        });
    });
}

function updateEvent(req, res, next) {
    //They cant be empty fields
    var cleanDescription = sanitizehtml(req.body.eventDescription, {
        allowedTags: sanitizehtml.defaults.allowedTags.concat(['iframe']),
        allowedAttributes: {
            a: ['href', 'name', 'target'],
            iframe: ['allowfullscreen', 'width', 'height', 'src', 'frameborder', 'allowfullscreen']
        }
    });

    var $ = cheerio.load(cleanDescription);
    $('iframe').each(function (i, elem) {
        var src = $(this).attr('src');
        //If src isn't youtube, then remove iframe
        const refReq = nodeurl.parse(src, true);
        if (refReq.host !== "www.youtube.com") {
            //Remove element
            $(this).remove();
        }
    });

    const evname = req.body.eventName;
    const evVenue = req.body.eventLocation;
    const evDescriptionHTML = cleanDescription;
    const startDate = {
        Date: JSON.parse(req.body.startDate).Date,
        Time: JSON.parse(req.body.startDate).Time
    };
    const endDate = {
        Date: JSON.parse(req.body.endDate).Date,
        Time: JSON.parse(req.body.endDate).Time
    };

    let posterPath = req.file == null ? `${posterPathDir}/${req.body.imgPath}` : `${posterPathDir}/${req.file.filename}`;


    const ev = new event({
        eventID: geneventID(),
        userID: JSON.parse(req.body.user).id,
        eventName: evname,
        eventLocation: evVenue,
        eventDescription: evDescriptionHTML,
        startDate: startDate,
        endDate: endDate,
        posterPath: posterPath
    });

    event.update({ eventaliasID: req.body.eventaliasID }, {
        $set: {
            eventName: evname,
            eventLocation: evVenue,
            eventDescription: evDescriptionHTML,
            startDate: startDate,
            endDate: endDate,
            posterPath: posterPath
        }
    }, (err) => {
        if (err) { return next(err); }
        logger.eventInfo.info(`Event ${evname} : ${req.body.eventaliasID} has been edited.`);
        return res.status(200).json(`You have successfully edited ${evname}`);
    });

}