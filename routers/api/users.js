'use strict';
var util = require('util');
var user = require("../../models/user");
var express = require("express");
var logger = require('../../utils/logger.js');

var router = express.Router();
module.exports = router;

router.get("/:id", function(req, res, next) {
    user.findById({ _id: req.params.id }, function(err, usr) {
        if (err) { return next(err); }

        if (!usr) { return res.sendStatus(404); }

        return res.json(usr);
    });
});

router.get("/delete/:id", function(req, res, next) {
    user.findByIdAndRemove({ _id: req.params.id }, function(err, usr) {
        if (err) { return next(err); }
        if (!usr) { return res.sendStatus(404); }

        logger.userInfo.info(`User ${usr.name.first} ${usr.name.last} has been successfully removed.`);
        return res.sendStatus(200);
    });
});


//Create new user
router.post("/create", function(req, res, next) {
    var usr = new user({
        userID: req.body.UID,
        name: { first: req.body.fname, last: req.body.lname },
        email: req.body.email,
        phoneNumber: req.body.phone,
        createdAt: Date.now()
    });

    usr.save(function(err) {
        if (err) { return next(err); } else {
            logger.userInfo.info(`User ${usr.name.first} ${usr.name.last} has been successfully added.`);
            return res.json(usr);
        }
    });
});