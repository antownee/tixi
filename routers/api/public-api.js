'use strict';
const mongoose = require('mongoose');
const order = require("../../models/order");
const event = require("../../models/event");
const ticket = require("../../models/ticketType");
const express = require("express");
const shortid = require('shortid');
const config = require("../../config/config");
const logger = require('../../utils/logger');
const promise = require('bluebird');
const lipishaPromise = promise.promisifyAll(require('../../utils/lipisha'));
const nodeurl = require('url');
const workers = require('../../utils/workers/workers');
const _ = require('lodash');
const async = require('async');


//promisifyAll
promise.promisifyAll(mongoose);

var router = express.Router();
module.exports = router;

//Generate orderID
function genorderID() {
    shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
    return shortid.generate().concat(shortid.generate());
}

function Padder(len, pad) {
    if (len === undefined) {
        len = 1;
    } else if (pad === undefined) {
        pad = '0';
    }

    var pads = '';
    while (pads.length < len) {
        pads += pad;
    }

    this.pad = function (what) {
        var s = what.toString();
        return pads.substring(0, pads.length - s.length) + s;
    };
}
var pd = new Padder(6);

router.get("/loading", function (req, res, next) {
    res.render('loading.pug', {
        header: "Finishing up..",
        message: "Finishing up.."
    }); //render complete page to the modal
});

router.get("/complete", function (req, res, next) {
    res.render('ordercomplete.pug', {
        header: "ORDER COMPLETE",
        message: "Once we confirm your order, you should find your tickets in your email. Thanks.",
        complete: true
    }); //render complete page to the modal
});

router.get("/incomplete", function (req, res, next) {
    res.render('ordercomplete.pug', {
        header: "OOPS!",
        message: "We're sorry, there seems to be a problem on our end. Care to try again later?",
        complete: false
    }); //render complete page to the modal
});

router.get("/email", function (req, res, next){
    res.render('email3.pug', {
        festivalName: "Solange: Live at The Alchemist",
        fullName: "Ivy Gathii"
    });

})


router.post("/create/card", function (req, res, next) {
    var ordrID = genorderID();

    //LINK SHIT
    const refLink = req.get('Referrer'); //reference link
    const refReq = nodeurl.parse(refLink, true);
    const channel = refReq.query.utm_channel || "SITE";
    const evAl = refReq.pathname.replace('/events/', '');
    const evAlias = parseInt(evAl);
    let amnt = 0;

    event.findOne({ eventaliasID: evAlias }, (err, evnt) => {
        if (err) { return res.redirect("/"); }

        if (!evnt) { return res.redirect("/"); }

        
        calculateTotalOrderAmount(req.body.tickets)
            .then((a) => {
                amnt = a.amount;
                return saveOrderTransaction(ordrID, a.amount, a.tickets, refLink, channel, evAlias, "CARD", req, res, next);
            })
            .then(() => {
                var ccdetails = {
                    cno: req.body.cno,
                    cvc: req.body.cvc,
                    ex: req.body.exp,
                    amnt: amnt,
                    ph: req.body.phone,
                    fullname: req.body.name,
                    accNo: config.lipisha.accountNumber,
                };
                return lipishaPromise.authoriseLipisha(ccdetails);
            })
            .then(function (authResp) {
                //Log in the transaction flow log
                res.sendStatus(200);
                return lipishaPromise.completeLipisha(authResp);
            })
            .then(function (compRes) {
                var refCode = compRes.content.transaction_index;
                return updateOrderTransaction(refCode, ordrID);
            })
            .then(function (ordr) {
                var jobdata = {
                    title: `Sending PDF tickets to ${ordr.email}`,
                    order: ordr,
                    event: evnt
                };
                return workers.create("emails", jobdata);
            })
            .catch(function (err) {
                return next(err);
            });
    });
});

router.post("/create/mobile", function (req, res, next) {
    //First save transaction

    var ordrID = genorderID();
    //var eventID = req.body.eventID;

    //Extract route parameters and determine if channel is via twitter, facebook, instagram or email
    //SAMPLE LINK: http://localhost:4000/events/koroga+festival?utm_channel=twitter&utm_num=74534
    //http://localhost:4000/events/1001?utm_channel=twitter - SAMPLE EVENT PAGE LINK
    var refLink = req.get('Referrer'); //reference link
    var refReq = nodeurl.parse(refLink, true);
    var channel = refReq.query.utm_channel || "SITE";
    var evAlias = refReq.pathname.replace('/events/', '');

    evAlias = parseInt(evAlias);

    event.findOne({ eventaliasID: evAlias }, (err, evnt) => {
        if (err) { return res.send("No such event was found") };


        //Mobiledetails
        var mobileDetails = {
            accountNumber: config.lipisha.accountNumber,
            mobileNumber: req.body.phone,
            method: "Paybill (M-Pesa)",
            amount: req.body.amount,
            orderID: ordrID
        }

        saveOrderTransaction(ordrID, refLink, channel, evAlias, "MPESA", req, res, next);

        lipishaPromise.processMobilePayment(mobileDetails)
            .then(function (mobileResponse) {

                var refCode = mobileResponse.content.reference;
                //mobileResponse.status == success
                //mobileResponse.content.reference
                res.sendStatus(200);
                return updateOrderTransaction(refCode, ordrID);
            })
            .then(function (ordr) {
                var jobtitle = `Sending PDF tickets to ${ordr.email}`;
                var jobdata = { title: jobtitle, order: ordr, event: evnt };
                return workers.create("emails", jobdata);
                //return sendEmail(ordr, evnt);
            })
            .catch(function (err) {
                return next(err);
            });
    });
});


function saveOrderTransaction(ordrID, amount, tickets, refLink, channel, evAlias, pMode, req) {
    return new Promise((resolve, reject) => {
        //SANITIZE
        var ordr = new order({
            orderID: ordrID,
            eventID: evAlias,
            datePurchased: Date.now(),
            name: {
                first: req.body.name
            },
            email: req.body.email,
            phoneNumber: req.body.phone,
            amount: amount,
            orderComplete: false,
            paymentMode: pMode,
            paymentReferenceCode: "",
            tickets: tickets,
            referralLink: refLink,
            referralChannel: channel,
            createdAt: Date.now(),
            updatedAt: Date.now()
        });

        ordr.save(function (err, o) {
            if (err) {
                return reject(err);
            }
            logger.transactionLogger.info("ORDERID: " + o.orderID + " SAVED.");
            return resolve();
        });
    });
}

function updateOrderTransaction(refCode, ordrID) {
    return new Promise(function (resolve, reject) {
        order.findOneAndUpdate({
            orderID: ordrID.toUpperCase()
        }, {
                $set: {
                    "paymentReferenceCode": refCode,
                    "orderComplete": true
                }
            }, {
                returnNewDocument: true,
                new: true
            }, function (err, ordr) {
                if (err) {
                    reject(err);
                } else {
                    logger.transactionLogger.info("ORDERID: " + ordr.orderID + " UPDATED.");
                    resolve(ordr);
                }
            });
    });
};

function calculateTotalOrderAmount(tickets) {
    return new Promise((resolve, reject) => {
        let orderTotal = 0;
        let newTickets = []

        async.waterfall([
            (cb) => {
                async.each(tickets, (tck, cbk) => {
                    var id = tck.id;
                    ticket.findOne({ ticketTypeID: id })
                        .then((res) => {
                            if (!res) { return cb(new Error('No ticket found')); }
                            orderTotal += res.price;
                            newTickets.push({
                                ticketType: res.name,
                                price: res.price
                            });
                            cbk();
                        }).catch((err) => {
                            if (err) { return cb(err); }
                        })
                },
                    (err) => {
                        if (err) { return cb(err); }
                        //proceed
                        cb(null);
                    })
            }
        ], (err, tot) => {
            if (err) {
                return reject(err);
            }

            return resolve({
                amount: orderTotal,
                tickets: newTickets
            });
        });
    });

}