'use strict';
const ticket = require("../../models/ticketType");
const express = require("express");
const logger = require('../../utils/logger.js');


var router = express.Router();
module.exports = router;

router.get("/all", function(req, res, next) {
    ticket.find({}, function(err, tcks) {
        if (err) { return next(err); }

        if (!tcks) { return res.sendStatus(404); }

        return res.json(tcks);
    });
});


router.get("/:id", function(req, res, next) {
    ticket.findById({ _id: req.params.id }, function(err, tck) {
        if (err) { return next(err); }

        if (!tck) { return res.sendStatus(404); }

        return res.json(tck);
    });
});


router.get("/delete/:id", function(req, res, next) {
    ticket.findOneAndRemove({ ticketTypeID : req.params.id }, function(err, tck) {
        if (err) { return next(err); }
        if (!tck) { return res.sendStatus(404); }

        logger.ticketInfo.info(`Ticket type ${tck.name} has been removed`);
        return res.json(tck);
    });
});


//Create new ticket
router.post("/create", function(req, res, next) {
    var tck = new ticket({
        name: req.body.ticketName, //VIP, early bird, advance,gate, vvip etc etc
        price: req.body.price,
        quantity: req.body.quantity,
        haltSales: false,
        eventaliasID: req.body.eventalias
    });

    tck.save(function(err) {
        if (err) { return next(err); } else {
            logger.ticketInfo.info(`Ticket type ${tck.name} has been added`);
            return res.json(tck);
        }
    });
});