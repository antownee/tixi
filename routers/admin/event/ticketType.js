'use strict';
var express = require("express");
var ticket = require("../../../models/ticketType");
var logger = require('../../../utils/logger.js');
var shortId = require('shortid');


var router = express.Router();
module.exports = router;

//Tickets
router.get("/", function(req, res, next) {
    //Load the ticket types available and bring them up into the view
    ticket.find({ eventaliasID: req.session.eventalias }, function(err, tck) {
        if (err) { next(err); }

        return res.render("event/ticketType.pug", {
            ticketTypes: tck,
            one: false,
            two: false,
            three: false,
            four: false,
            five: false,
            six: true,
            eventName: req.session.eventName
        });
    });
});

router.get("/:id", function(req, res, next) {
    ticket.findById({ _id: req.params.id }, function(err, tck) {
        if (err) { next(err); }

        if (!tck) {
            return res.sendStatus(404);
        }

        return res.json(tck);
    });
});

router.get("/delete/:id", function(req, res, next) {
    ticket.findByIdAndRemove({ _id: req.params.id }, function(err, tck) {
        if (err) { next(err); }
        if (!tck) {
            return res.sendStatus(404);
        }

        logger.ticketInfo.info(`Ticket type ${tck.name} has been removed`);
        return res.redirect("/admin/event/tickets");
    });
});


//Create new ticket
router.post("/create", function(req, res, next) {
    var tck = new ticket({
        name: req.body.ticketName, //VIP, early bird, advance,gate, vvip etc etc
        price: req.body.price,
        quantity: req.body.quantity,
        haltSales: false,
        eventaliasID: req.session.eventalias,
        ticketTypeID: shortId.generate()
    });

    tck.save(function(err) {
        if (err) { next(err); } else {
            logger.ticketInfo.info(`Ticket type ${tck.name} has been added`);
            return res.redirect("/admin/event/tickets");
        }
    });
});

//Update ticket
router.post("/edit/:id", function(req, res, next) {
    ticket.findById({ _id: req.params.id }, function(err, tck) {
        if (err) { next(err); }
        if (!tck) {
            return res.sendStatus(404);
        }

        tck.name = req.body.ticketName;
        tck.price = req.body.price;
        tck.quantity = req.body.quantity;

        tck.save(function(err) {
            if (err) {
                return next(err);
            }

            logger.ticketInfo.info(`Ticket type ${tck.name} has been updated`);
            return res.redirect("/admin/event/tickets");
        });
    });
});

router.get("/pause/:id", function(req, res, next) {
    var tID = req.params.id;

    ticket.update({ _id: tID }, { $set: { haltSales: true } }, (err) => {
        if (err) { return next(err); }
        return res.redirect("/admin/event/tickets");
    });
});

router.get("/resume/:id", function(req, res, next) {
    var tID = req.params.id;

    ticket.update({ _id: tID }, { $set: { haltSales: false } }, (err) => {
        if (err) { return next(err); }
        return res.redirect("/admin/event/tickets");
    });
});