var mongoose = require('mongoose');
var util = require('util');
var express = require("express");
var _ = require("lodash");


var router = express.Router();
module.exports = router;



router.get('/', (req,res,next) => {
    res.render('event/metrics/email.pug')
})


router.get('/modal/:id', (req,res,next) => {
    var id = req.params.id;
    res.render('event/metrics/emailmodal.pug');

    //Load the email.pug into modal
})