'use strict';
const express = require("express");
const mongoose = require("mongoose");
const async = require("async");
const _ = require("lodash");
const moment = require("moment");
const numeral = require('numeral');
//Models
const ticket = require("../../../models/ticketType");
const order = require("../../../models/order");
const pageview = require("../../../models/pageview");

const router = express.Router();
module.exports = router;

router.get("/", function(req, res, next) {
    dashboardPopulationSeries(req, res, next);
    //If pageviews or orders are empty, render an empty home page with onstructions to add tickets
    // order.count({eventID: req.session.eventalias}, (err, ocount) => {
    //     if (err) { return next(err) }
    //     pageview.count({ eventID: req.session.eventaliasID}, (err, pcount) => {
    //         if (err) { return next(err) }
    //         var orderCount = ocount;
    //         var viewCount = pcount;
    //         var k = viewCount;
    //         if (orderCount > 0 && pcount > 0) {
    //             dashboardPopulationSeries(req, res, next);
    //         } else {
    //             //Render page with information on how to proceed
    //             //res.send(200, "Nothing here")
    //             res.render('event/firstdash.pug')
    //         }
    //     })

    // })
})

function dashboardPopulationSeries(req, res, next) {
    const id = req.session.eventalias;
    async.series([
        function(cb) {
            getTotalTicketsSold(id, cb);
        },
        function(cb) {
            //Pageviews
            getPageViews(id, cb);
        },
        function(cb) {
            //Total sales
            getTotalOrderSales(id, cb)
        },
        function(cb) {
            //Total sales
            getWeeklyOrderSales(id, cb)
        },
        function(cb) {
            //Page view channels
            getPageviewchannels(id, cb)
        }
    ], function(err, results) {
        if (err) { return next(err) }
        const weeklyTickets = _.map(results[0].weekTickets, (res) => {
            return res.times;
        });
        const pageViews = _.map(results[1].pageViews, (res) => {
            return res.times;
        });
        const weeklyOrders = _.map(results[3].weeklyOrderSales, (res) => {
            return res.total;
        })
        const h = [];
        const g = _.each(results[3].groupedWeeklyChannelConversions, (o) => {
            var k = _.map(o.weekStats, (m) => {
                return m.count;
            })
            h.push({ day: o.day, weekStats: k });
        });
        const gwcc = _.keyBy(h, function(o) {
            return o.day;
        });

        //format
        const totalTickets = numeral(results[0].totalTickets).format("0,0");
        const ticketsByType = results[0].groupedTickets;
        const totalPageViews = numeral(results[1].totalViews).format("0,0");
        const orderSalesTotal = numeral(results[2].totalAmount).format("0,0");
        const groupedChannelConversions = results[3].groupedChannelReferrals;
        const totalOrdersMade = results[3].totalOrdersMade;
        const lastFiveOrders = results[3].lastFiveOrders;
        const pageViewchannelCount = results[4].channelCount;


        return res.render("event/dash.pug", {
            eventName: req.session.eventName,
            eventaliasID: req.session.eventalias,
            host: req.headers.host,
            totalTickets: totalTickets,
            ticketsByType: ticketsByType,
            weeklyTickets: weeklyTickets,
            weeklyOrders: weeklyOrders,
            pageViews: pageViews,
            totalPageViews: totalPageViews,
            channelCount: pageViewchannelCount,
            groupedChannelConversions: groupedChannelConversions,
            groupedWeeklyChannelReferrals: gwcc,
            orderSalesTotal: orderSalesTotal,
            totalOrdersMade: totalOrdersMade,
            lastFiveOrders: lastFiveOrders,
        });
    })
}

function getTotalTicketsSold(id, cb) {
    order.aggregate([
        { $match: { eventID: id } },
        { $project: { tickets: 1 } }, /* select the tickets field as something we want to "send" to the next command in the chain */
        { $unwind: '$tickets' }, /* this converts arrays into unique documents for counting */
        {
            $group: { /* execute 'grouping' */
                _id: '$tickets',
                /* using the 'token' value as the _id */
            }
        }
    ], (err, totalTickets) => {
        if (err) { return cb(err) };

        //Get ticket count by type. VIP blah blah
        const g = _.chain(totalTickets)
            .countBy((itm) => {
                return itm._id.ticketType;
            }).map((count, type) => {
                return {
                    type: type,
                    count: count
                }
            }).value();

        const gt = _.chain(g)
            .values(g)
            .map((itm) => {
                var h = [];
                h.push(itm.type);
                h.push(itm.count)
                return h;
            }).value();

        //Get tickets sold over last 7 days
        //const w = _.sortBy(totalTickets, '_id.purchaseDate').slice(0, 7);
        const weeklyTix = _.chain(totalTickets)
            .map((itm) => {
                return itm._id;
            }).groupBy((itm) => {
                return moment(itm.purchaseDate).startOf('day').format("dddd");
            }).map((group, day) => {
                return {
                    day: day,
                    times: group.length
                };
            })
            .value();

        const wt = weekTicketsFiller(weeklyTix);

        const tickets = {
            groupedTickets: gt,
            weekTickets: wt,
            totalTickets: totalTickets.length
        }

        return cb(null, tickets)
    })
}

function weekTicketsFiller(tickets) {
    var week = [{ day: "Monday", index: 1 }, { day: "Tuesday", index: 2 }, { day: "Wednesday", index: 3 }, { day: "Thursday", index: 4 }, { day: "Friday", index: 5 }, { day: "Saturday", index: 6 }, { day: "Sunday", index: 7 }, ];
    const k = _.map(tickets, (itm) => {
        return itm.day;
    });
    for (var i = 0; i < week.length; i++) {
        var weekDay = week[i].day;
        if (!_.includes(k, weekDay)) {
            tickets.push({ day: weekDay, times: 0 });
        }
    }
    const j = _.sortBy(tickets, (itm) => {
            const k = _.find(week, (day) => {
                return day.day === itm.day;
            });
            return k.index;
        })
        //Sort days
    return j;
}


function getPageViews(id, cb) {
    const today = moment().format();
    const weekAgo = moment(today).subtract(1, 'week');

    pageview.find({
        eventID: id,
        dateVisited: {
            $gte: weekAgo.toDate(),
            $lt: today
        }
    }, (err, res) => {
        if (err) { return cb(err) }

        const finalViews = _.chain(res)
            .groupBy((itm) => {
                return moment(itm.dateVisited).startOf('day').format("dddd");
            })
            .map((group, day) => {
                return {
                    day: day,
                    times: group.length
                }
            })
            .value();

        var k = weekViewsFiller(finalViews);

        return cb(null, { pageViews: k, totalViews: res.length })
    })
}

function weekViewsFiller(views) {
    var week = [{ day: "Monday", index: 1 }, { day: "Tuesday", index: 2 }, { day: "Wednesday", index: 3 }, { day: "Thursday", index: 4 }, { day: "Friday", index: 5 }, { day: "Saturday", index: 6 }, { day: "Sunday", index: 7 }, ];
    const k = _.map(views, (itm) => {
        return itm.day
    })
    for (var i = 0; i < week.length; i++) {
        var weekDay = week[i].day;
        if (!_.includes(k, weekDay)) {
            views.push({ day: weekDay, times: 0 });
        }
    }
    const j = _.sortBy(views, (itm) => {
            const k = _.find(week, (day) => {
                return day.day == itm.day;
            })
            return k.index;
        })
        //Sort days
    return j;
}


function getTotalOrderSales(id, cb) {
    order.aggregate([
        { $match: { eventID: id } },
        {
            $group: {
                _id: null,
                amount: { $sum: "$amount" }
            }
        }
    ], (err, res) => {
        if (err) { return cb(err) };
        const total = _.first(res);
        if (total == undefined) {
            return cb(null, { totalAmount: 0 });
        }
        return cb(null, { totalAmount: total.amount });
    })
}

function getWeeklyOrderSales(id, cb) {
    const today = moment().format();
    const weekAgo = moment(today).subtract(1, 'week');

    order.find({
        eventID: id,
        datePurchased: {
            $gte: weekAgo.toDate(),
            $lt: today
        }
    }, (err, res) => {
        if (err) { return cb(err) };

        const lastFiveOrders = _.chain(res)
            .orderBy((itm) => {
                return itm.datePurchased;
            }).map((ordr) => {
                return {
                    orderNumber: ordr.orderID,
                    ticketsNum: ordr.tickets.length,
                    orderTotal: ordr.amount
                }
            }).take(5)
            .value();

        const orders = _.chain(res)
            .groupBy((itm) => {
                return moment(itm.datePurchased).startOf('day').format("dddd");
            })
            .map((group, day) => {
                return {
                    day: day,
                    total: _.sumBy(group, "amount")
                }
            })
            .value();

        const channelReferrals = _.chain(res)
            .countBy((itm) => {
                return itm.referralChannel;
            })
            .map((count, channel) => {
                return {
                    channel: channel,
                    count: count
                }
            })
            .value();

        const totalOrdersMade = _.chain(channelReferrals)
            .sumBy((itm) => {
                return itm.count
            })
            .value();
        //Weekly channel conversions
        const wcc = _.groupBy(res, (itm) => {
            return itm.referralChannel;
        });

        var cpw = [];
        var channelPerWeek = [];

        _.each(wcc, (group, channel) => {
            const k = _.chain(group)
                .groupBy((l) => {
                    return moment(l.datePurchased).startOf('day').format("dddd");
                }).map((group, h) => {
                    return {
                        day: h,
                        count: group.length
                    }
                })
                .value();
            cpw.push({ day: channel, weekStats: k })
        })

        _.each(cpw, (itm) => {
            channelPerWeek.push({ day: itm.day, weekStats: weekConversionFiller(itm.weekStats) })
        })

        //var j = nest(res, ['referralChannel', 'datePurchased']);
        var k = weekSalesFiller(orders);

        return cb(null, { lastFiveOrders: lastFiveOrders, weeklyOrderSales: k, groupedChannelReferrals: channelReferrals, groupedWeeklyChannelConversions: channelPerWeek, totalOrdersMade: totalOrdersMade })
    })
}

function weekConversionFiller(finalOrder) {
    var week = [{ day: "Monday", index: 1 }, { day: "Tuesday", index: 2 }, { day: "Wednesday", index: 3 }, { day: "Thursday", index: 4 }, { day: "Friday", index: 5 }, { day: "Saturday", index: 6 }, { day: "Sunday", index: 7 }, ];
    const k = _.map(finalOrder, (itm) => {
        return itm.day
    })
    for (var i = 0; i < week.length; i++) {
        var weekDay = week[i].day;
        if (!_.includes(k, weekDay)) {
            finalOrder.push({ day: weekDay, count: 0 });
        }
    }
    const j = _.sortBy(finalOrder, (itm) => {
            const k = _.find(week, (day) => {
                return day.day == itm.day;
            })
            return k.index;
        })
        //Sort days
    return j;
}

var nest = function(seq, keys) {
    if (!keys.length)
        return seq;
    var first = keys[0];
    var rest = keys.slice(1);
    return _.mapValues(_.groupBy(seq, first), function(value) {
        return nest(value, rest)
    });
};

function weekSalesFiller(finalOrder) {
    var week = [{ day: "Monday", index: 1 }, { day: "Tuesday", index: 2 }, { day: "Wednesday", index: 3 }, { day: "Thursday", index: 4 }, { day: "Friday", index: 5 }, { day: "Saturday", index: 6 }, { day: "Sunday", index: 7 }, ];
    const k = _.map(finalOrder, (itm) => {
        return itm.day
    })
    for (var i = 0; i < week.length; i++) {
        var weekDay = week[i].day;
        if (!_.includes(k, weekDay)) {
            finalOrder.push({ day: weekDay, total: 0 });
        }
    }
    const j = _.sortBy(finalOrder, (itm) => {
            const k = _.find(week, (day) => {
                return day.day == itm.day;
            })
            return k.index;
        })
        //Sort days
    return j;
}



function getPageviewchannels(id, cb) {
    pageview.find({ eventID: id }, "channel", (err, res) => {
        if (err) { return cb(err) }
        const chnlcount = _.countBy(res, 'channel');
        return cb(null, { channelCount: chnlcount })
    })
}

//Email (Monday:6 Tuesday 7) Twitter (Monday:4) etc






//Email - 4 FB - 6 etc
// function getGroupedConversionChannels(id, cb) {
//     order.find({ eventID: id }, "referralChannel", (err, res) => {
//         if (err) { return cb(err) };

//         return cb(null, res);

//     })
// }

//Get total tickets count - DONE
//Get ticket count by type. VIP blah blah - DONE
//Get sold tickets (Count tickets within orders) - DONE
//Get tickets sold over last 7 days. - DONE
//Get pageview count - DONE
//Get pageview count over 7 days
//Get total sales (Order total) - DONE
//Get sales over 7 days - DONE
//Get order referrals. Count which ones are from twitter, facebook, instagram and email