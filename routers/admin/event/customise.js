'use strict';
const express = require("express");
const event = require("../../../models/event");
const sanitizehtml = require('sanitize-html');
const shortid = require('shortid');
const cheerio = require('cheerio');
const nodeurl = require('url');
const logger = require('../../../utils/logger.js');


var router = express.Router();
module.exports = router;

function geneventID() {
    shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
    return shortid.generate();
}

//Tickets

router.get("/first", function(req, res, next) {
    return res.render('event/customise.pug', {
        event: {
            eventDescription: ""
        },
        firstTime: true
    });
});


router.get("/", function(req, res, next) {
    //Look up the event.
    //If it exists, load the fields
    event.findOne({ eventaliasID: req.session.eventalias }, (err, ev) => {
        if (err) { return next(err); }
        if (!ev) {
            return res.render('event/customise.pug', {
                event: {
                    eventDescription: ""
                },
                firstTime: true
            });
        }

        return res.render('event/customise.pug', { event: ev, host: req.headers.host, eventName: req.session.eventName });
    });
});

router.uploadImage = function(req, res, next) {
    //Check for empty fields
    req.assert('eventVenue', 'Event venue cannot be empty').notEmpty();
    req.assert('eventName', 'Event name cannot be empty').notEmpty();
    req.assert('editor', 'Kindly enter event description').notEmpty();
    req.assert('startDate', 'Start date cannot be empty').notEmpty();
    req.assert('startTime', 'Kindly pick a starting time').notEmpty();
    req.assert('endDate', 'End date cannot be empty').notEmpty();
    req.assert('endTime', 'Kindly pick an ending time').notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        req.flash('errors', errors);
        return res.redirect('/admin/event/customise');
    }

    if (req.session.eventalias != null) {
        //UPDATE
        //Load the filepath
        return updateEvent(req, res, next);

    } else {
        //POST
        return postEvent(req, res, next);
    }
};


function postEvent(req, res, next) {
    //SANITIZE
    var cleanDescription = sanitizehtml(req.body.editor, {
        allowedTags: sanitizehtml.defaults.allowedTags.concat(['iframe']),
        allowedAttributes: {
            a: ['href', 'name', 'target'],
            iframe: ['allowfullscreen', 'width', 'height', 'src', 'frameborder', 'allowfullscreen']
        }
    });

    var $ = cheerio.load(cleanDescription);
    $('iframe').each(function(i, elem) {
        var src = $(this).attr('src');
        //If src isn't youtube, then remove iframe
        const refReq = nodeurl.parse(src, true);
        if (refReq.host !== "www.youtube.com") {
            //Remove element
            $(this).remove();
        }
    });

    const evname = req.body.eventName;
    const evVenue = req.body.eventVenue;
    const pPath = req.session.fpath;
    const evDescriptionHTML = cleanDescription;
    const startDate = {
        Date: req.body.startDate,
        Time: req.body.startTime
    };
    const endDate = {
        Date: req.body.endDate,
        Time: req.body.endTime
    };


    const ev = new event({
        eventID: geneventID(),
        userID: req.user._id,
        eventName: evname,
        eventLocation: evVenue,
        eventDescription: evDescriptionHTML,
        startDate: startDate,
        endDate: endDate,
        posterPath: pPath
    });

    ev.save(function(err, ev) {
        if (err) { return next(err); }

        logger.eventInfo.info(`Event ${ev.eventName} : ${ev.eventID} has been added.`);
        //Flash with successfully added
        req.flash('success', { msg: `You have successfully added ${ev.eventName}` });
        req.session.eventalias = ev.eventaliasID;
        req.session.eventName = ev.eventName;
        return res.redirect('/admin/event/customise');
    });
}

function updateEvent(req, res, next) {
    //They cant be empty fields
    var d = sanitizehtml(req.body.editor, {
        allowedTags: sanitizehtml.defaults.allowedTags.concat(['iframe']),
        allowedAttributes: {
            a: ['href', 'name', 'target'],
            iframe: ['allowfullscreen', 'width', 'height', 'src', 'frameborder', 'allowfullscreen']
        }
    });

    var $ = cheerio.load(d);
    $('iframe').each(function(i, elem) {
        var src = $(this).attr('src');
        //If src isn't youtube, then remove iframe
        const refReq = nodeurl.parse(src, true);
        if (refReq.host !== "www.youtube.com") {
            //Remove element
            $(this).remove();
        }
    });

    event.update({ eventaliasID: req.session.eventalias }, {
        $set: {
            eventName: req.body.eventName,
            eventLocation: req.body.eventVenue,
            eventDescription: d,
            startDate: {
                Date: req.body.startDate,
                Time: req.body.startTime
            },
            endDate: {
                Date: req.body.endDate,
                Time: req.body.endTime
            },
            posterPath: req.session.fpath || req.body.pPath
        }
    }, (err) => {
        if (err) { return next(err); }
        req.flash('success', { msg: `You have successfully edited ${req.body.eventName}` });
        logger.eventInfo.info(`Event ${req.body.eventName} : ${req.body.eventID} has been edited.`);
        req.session.eventName = req.body.eventName;
        return res.redirect('/admin/event/customise');
    });

}