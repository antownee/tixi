'use strict'
const express = require("express");
const logger = require('../../../utils/logger')
const requireIp = require('request-ip')


//Define before the users and purchases routers
const router = express.Router();


//DASHBOARD
var dashboardRouter = require("./dashboard");
router.use("/", dashboardRouter)

//Social media shit here!
//Metrics
var ticketRouter = require("./metrics/metrics");
router.use("/metrics", ticketRouter);

//Tickets
var ticketRouter = require("./ticketType");
router.use("/tickets", ticketRouter);

//Customize
var customiseRouter = require("./customise");
router.use("/customise", customiseRouter);

//Orders
var orderRouter = require("./orders");
router.use("/orders", orderRouter);

//Account
var accountRouter = require("./account");
router.use("/account", accountRouter);

module.exports = router;
