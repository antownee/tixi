'use strict';
var util = require('util');
var express = require("express");
var user = require("../../../models/user");
var _ = require("lodash");


var router = express.Router();
module.exports = router;


router.get("/", (req, res, next) => {
    //Look up user
    var id = req.session.userID;
    user.findOne({ _id: id }, (err, usr) => {
        if (err) { return next(err) }
        if (!res) { return res.sendStatus(404) }

        //Render account page with user details
        var u = {
            name: usr.name,
            email: usr.email,
            phoneNumber: usr.phoneNumber,
            createdAt: usr.createdAt
        };
        return res.render('event/account.pug', { user: u, eventName: req.session.eventName });

    })
})