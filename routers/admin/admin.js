'use strict';
var express = require("express");
const logger = require('../../utils/logger');
const requireIp = require('request-ip');

//Models
const event = require("../../models/event");

//Define before the users and purchases routers
var router = express.Router();

//Logger stuff
router.use(logger.adminLogger);
router.use(requireIp.mw());

//HOME, INDEX
router.get("/", function(req, res) {
    res.redirect("/admin/event");
});

router.get("/event/list", function(req, res, next) {
    event.find({ userID: req.user._id }, (err, evnts) => {
        if (err) { return next(err); }
        if (evnts.length === 0) {
            return res.render('event/eventlist.pug', { events: evnts, firstTime: true });
        }
        return res.render('event/eventlist.pug', { events: evnts });
    });
});

//EVENT
var eventRouter = require("../admin/event/event");
router.use("/event", eventRouter);


//ACCEPT INCOMING LINK FROM SELECTED EVENT
router.get('/event/select/:id', (req, res, next) => {
    var evID = req.params.id;
    req.session.eventalias = evID;
    event.findOne({ eventaliasID: evID }, (err, ev) => {
        if (err) { return next(err); }
        req.session.eventName = ev.eventName;
        req.session.userID = ev.userID;
        return res.redirect('/admin/event');
    });
});


router.get('/event/delete/:id', (req, res, next) => {
    var evID = req.params.id;
    event.findOneAndRemove({ eventaliasID: evID }, (err, ev) => {
        if (err) { return next(err); }
        return res.redirect('/admin/event/list');
    });
});
module.exports = router;