'use strict';
const express = require("express");

let router = express.Router();
module.exports = router;


router.get('/', (req, res) => {
     return res.render('index');
});
