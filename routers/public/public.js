'use strict';
const express = require("express");
const pdf = require('html-pdf');
const pug = require('pug');
const event = require("../../models/event");
const _ = require('lodash');
const utls = require('../../utils/utils.js');

var router = express.Router();
module.exports = router;

router.get('/', (req, res, next) => {
     return res.send("OK");
});


//Events
var eventRouter = require("../public/events");
router.use("/events", eventRouter);




// router.get("/mail", function(req, res) {
//     res.render("email.pug");
// });

// router.get("/ticket", function(req, res) {
//     res.render('ticket.pug', {
//         ticketID: "R1EYJNK1XRYXVJJ31JX",
//         ticketType: "Advance",
//         price: "KES 2500",
//         evntName: "BURNING MAN FESTIVAL",
//         location: "Arboretum Grounds, Nairobi ",
//         date: "JANUARY 12, 2012 8:00 PM"
//     });
// });


// router.get("/pdf", (req, res, next) => {
//     var compiledTicket = pug.compileFile(__dirname + "/../../views/ticket.pug");
//     var ticketHtml = compiledTicket({
//         ticketID: "R1EYJNK1XRYXVJJ31JX",
//         ticketType: "Advance",
//         price: "2500",
//         evntName: "BURNING MAN FESTIVAL",
//         location: "Arboretum Grounds, Nairobi ",
//         date: "JANUARY 12, 2012 8:00 PM"
//     });

//     const port = process.env.PORT;
//     pdf.create(ticketHtml, {
//         "format": "Legal",
//         "base": 'http://localhost:' + port,
//         "orientation": "landscape",
//     }).toFile('./ticket_test.pdf', function(err, pth) {
//         if (err) return next(err);
//         res.sendStatus(200);
//     });
// })
