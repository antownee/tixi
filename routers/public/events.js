'use strict';
const express = require("express");
const pageview = require("../../models/pageview");
const event = require("../../models/event");
const ticket = require("../../models/ticketType");
const utls = require("../../utils/utils.js");
const router = express.Router();
module.exports = router;


router.use(express.static("public"));


//Remember to do some ajax, jquery call handling here for event searching.
//Also render first few events

router.get("/all", (req, res, next) => {
    event.find({}, (err, evs) => {
        if (err) { return next(err); }

        if (!evs) { return res.send(404, "No events found!"); }

        res.render('event/public-event-list.pug', { events: evs });
    });
});

router.get("/:eventID", function(req, res, next) {
    const evID = parseInt(req.params.eventID);

    //If it's not there, invalid link
    if(isNaN(evID)){
        return res.redirect('/');
    }
    
    event.findOne({ eventaliasID: evID }, (err, evnt) => {
        if (err) { return next(err); }
        if (!evnt) {
            return res.redirect('/');
        }
        ticket.find({ eventaliasID: evID }, (err, tcks) => {
            if (err) { return next(err); }
            //Also add ticketType object
            var longDate = utls.getDate(evnt.startDate.Date, evnt.startDate.Time);
            res.render("event/eventpage2.pug", { event: evnt, tickets: tcks, longDate: longDate });
            savePageView(req, next);
        });
    });
});


function savePageView(req, next) {
    const link = req.originalUrl;
    const channel = req.query.utm_channel;
    const evAlias = req.params.eventID;

    var pv = new pageview({
        urlLong: link,
        channel: channel,
        eventID: evAlias,
        dateVisited: Date.now()
    });

    pv.save(function(err) {
        if (err) {
            return next(err);
        }
    });
}