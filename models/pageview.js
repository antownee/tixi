const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pageViewSchema = new Schema({
    urlLong: {type: String, index:true},
    channel: String,
    eventID: String,
    dateVisited: Date
});


const pageView = mongoose.model("PageView",pageViewSchema);
module.exports = pageView;