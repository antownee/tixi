const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const emailSchema = new Schema({
    emailID: {type: String, unique:true, index:true},
    emailBody: String,
    totalSent: Number,
    totalOpens: Number,
    totalClicks: Number,
    totalConversions: Number,
    referralLink: String
});


const email = mongoose.model("Email",emailSchema);
module.exports = email;