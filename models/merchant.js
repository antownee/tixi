var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var merchantSchema = new Schema({
    merchantID: {type: String, unique:true, index:true},
    custodianName: {
        first: String,
        last: String
    },
    eventID: String,
    businessName: String,
    standName: String,
    email: String,
    phoneNumber: String,
    createdAt: Date,
    updatedAt: Date
});

merchantSchema.pre("save", function (next) {
    var cd = new Date();
    this.updatedAt = cd;

    if(!this.createdAt){
        this.createdAt = cd;
    }
    next();
})

var merchant = mongoose.model("Merchant",merchantSchema);
module.exports = merchant;