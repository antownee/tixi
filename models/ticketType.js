'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shortId = require('shortid');

var ticketTypeSchema = new Schema({
    name: String, //VIP, early bird, advance,gate, vvip etc etc
    price: Number,
    quantity: Number,
    haltSales: Boolean,
    eventaliasID: { type: String, index: true },
    ticketTypeID: { type: String, index: true }
});

ticketTypeSchema.pre("save", function(next){
    this.ticketTypeID = shortId.generate();
    next();
});


var ticketType = mongoose.model("TicketType", ticketTypeSchema);
module.exports = ticketType;