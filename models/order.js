'use strict';
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;


function toUpper(f) {
    return f.toUpperCase();
}

//Generate ticketID
function genticketID() {
    shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
    return shortid.generate();
    //Proposal
    //orderID + '-' + shortid.generate()
}


var ticketSchema = new Schema({
    ticketID: { type: String, unique: true, index: true, set: toUpper },
    orderID: String,
    price: Number,
    ticketType: String,
    purchaseDate: Date
});

var orderSchema = new Schema({
    orderID: { type: String, unique: true, index: true, set: toUpper },
    eventID: String, //Alias
    datePurchased: Date,
    name: {
        first: String,
        last: String
    },
    email: String,
    phoneNumber: String,
    amount: Number,
    orderComplete: Boolean,
    paymentMode: String,
    paymentReferenceCode: String, //MPESA ref number or card transaction code
    cardNumber: Number, //Can be null if MPESA
    tickets: [ticketSchema], //An array of ticket objects (TicketID(unique),price,type)
    referralLink: String,
    referralChannel: String,
    createdAt: Date,
    updatedAt: Date
});


orderSchema.pre("save", function(next) {
    var cd = new Date();
    this.updatedAt = cd;

    if (!this.createdAt) {
        this.createdAt = cd;
    }

    var tkts = this.tickets;
    for (var index = 0; index < tkts.length; index++) {
        var tkt = tkts[index];
        tkt.ticketID = genticketID();
        tkt.orderID = this.orderID;
        tkt.purchaseDate = this.datePurchased;
    }

    next();
});

var order = mongoose.model("Order", orderSchema);
module.exports = order;