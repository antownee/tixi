var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var customerSchema = new Schema({
    name: {
        first: String,
        last: String
    },
    email: String,
    phoneNumber: String,
    wristbandID: String,
    createdAt: Date,
    updatedAt: Date,
    refunded: Boolean,
    balance: Number
});

customerSchema.pre("save", function (next) {
    var cd = new Date();
    this.updatedAt = cd;

    if(!this.createdAt){
        this.createdAt = cd;
    }
    next();
})

var customer = mongoose.model("Customer",customerSchema);
module.exports = customer;