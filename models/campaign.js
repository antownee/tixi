const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const campaignSchema = new Schema({
    campaignID: {type: String, unique:true, index:true},
    campaignName: String,
    campaignDescription: String,
    eventID: String,
    channelTwitter: Schema.Types.Mixed,
    channelFacebook: Schema.Types.Mixed,
    channelInstagram: Schema.Types.Mixed,
    channelEmail: Schema.Types.Mixed
});


const campaign = mongoose.model("Campaign",userSchema);
module.exports = campaign;