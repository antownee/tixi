'use strict';
const localStrategy = require('passport-local').Strategy;
const user = require('../models/user');


/**
 * Return the Passport Local Strategy object.
 */
module.exports = new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false,
    passReqToCallback: true
}, (req, email, password, done) => {
    const userData = {
        email: email.trim(),
        password: password.trim(),
        name: {
            first: req.body.fname.trim(),
            last: req.body.lname.trim(),
        },
    };

    const newUser = new user(userData);
    newUser.save((err) => {
        if (err) { return done(err); }

        return done(null);
    });
});