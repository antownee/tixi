'use strict';
const jwt = require('jsonwebtoken');
const localStrategy = require('passport-local').Strategy;
const config = require('../config/config');
const user = require('../models/user');


module.exports = new localStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        session: false,
        passReqToCallback: true
    },
    (req, email, password, done) => {
        user.findOne({ email: email.toLowerCase() }, (err, user) => {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(new Error(`${email} not found`), false, { msg: `${email} not found` });
            }
            user.comparePassword(password, (err, isMatch) => {
                if (err) {
                    return done(err);
                }
                if (isMatch) {
                    //Sign jwt
                    user = Object.assign(
                        {}, 
                        user, 
                        {
                            name: user.name, 
                            email: user.email, 
                            phoneNumber: user.phoneNumber,
                            id: user._id
                        });
                        
                    const payload = {
                        sub: user,
                        eventAlias: 1001
                    };
                    const token = jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (60 * 60), //1 hour 
                        data: payload
                    },config.jwt);

                    return done(null, token, user);
                }
                return done(new Error('Invalid email or password'), false, { msg: `Invalid email or password` });
            });
        });
    });