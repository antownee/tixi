'use strict';
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/tixi');

mongoose.connection.on('connected', function() {
    console.log('MongoDB connection established. ' + 'mongodb://localhost/tixi');
});

mongoose.connection.on('error', function() {
    console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
    process.exit(1);
});